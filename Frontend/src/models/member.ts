export default class Member 
{
    readonly id: string = "";
    public name: string = "";
    public surname: string = "";
    public email: string = "";

    constructor(id: string, name: string, surname: string, email: string) 
    {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
    }
}
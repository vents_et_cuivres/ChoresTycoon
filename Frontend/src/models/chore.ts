import Member from "./member";

export default class Chore 
{
    readonly id: string = "";
    public name: string = "";
    public description: string = "";
    public deadline: string = "";
    public completed: boolean = false;
    private _assignedMembers: Array<Member> = new Array<Member>();

    constructor(id: string, name: string, description: string, deadline: string) 
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.deadline = deadline;
    }

    public GetAssignedMembers(): Array<Member> {
        return this._assignedMembers;
    }

    public AssignMember(member: Member): void {
        let assignedMemberWithSameId = this._assignedMembers.find(m => m.id === member.id);
        if (assignedMemberWithSameId === undefined) {
            this._assignedMembers.push(member);
        }
    }

    public RemoveMember(member: Member): void {
        let assignedMemberIndex = this._assignedMembers.indexOf(member);
        if (assignedMemberIndex != -1) {
            this._assignedMembers.splice(assignedMemberIndex, 1);
        }
    }
}
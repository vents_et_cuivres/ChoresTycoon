import Chore from "../models/chore";
import Member from "../models/member";

export default interface ChorePort 
{
    CreateChore(name: string, description: string, deadline: string): Chore;
    GetChore(choreId: string): Chore|undefined;
    CompleteChore(choreId: string): void;
    ModifyChore(chore: Chore): void;
    AssignMemberToChore(chore: Chore, member: Member): void;
    DeleteChore(choreId: string) : void;
    RemoveMemberFromChore(chore: Chore, member: Member): void;
}
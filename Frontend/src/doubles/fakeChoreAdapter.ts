import Chore from "../models/chore";
import Member from "../models/member";
import ChorePort from "../ports/chorePort";

export default class FakeChoreAdapter implements ChorePort 
{
    private _lstChores: Array<Chore> = new Array<Chore>();
    private _nextId: number = 0;

    CreateChore(name: string, description: string, deadline: string): Chore {
        let id = this._nextId.toString();
        this._nextId++;

        let chore = new Chore(id, name, description, deadline);
        this._lstChores.push(chore);
        return chore;
    }

    GetChore(choreId: string): Chore|undefined {
        return this._lstChores.find(c => c.id === choreId);
    }

    CompleteChore(choreId: string): void {
        let chore: Chore = this._lstChores.find(c => c.id === choreId);
        let choreIndex = this._lstChores.indexOf(chore);

        chore.completed = true;

        this._lstChores.splice(choreIndex, 1);
        this._lstChores.push(chore);
    }

    ModifyChore(chore: Chore): void {
        let choreIndex = this._lstChores.indexOf(chore);
        this._lstChores.splice(choreIndex, 1);
        this._lstChores.push(chore);
    }

    AssignMemberToChore(chore: Chore, member: Member): void {
        let choreWithSameId: Chore =  this._lstChores.find(c => c.id === chore.id);
        if (choreWithSameId != undefined) {
            let choreIndex = this._lstChores.indexOf(chore);
            this._lstChores.splice(choreIndex, 1);

            chore.AssignMember(member);
            this._lstChores.push(chore);
        }
    }

    DeleteChore(choreId: string): void {
        let chore: Chore = this._lstChores.find(c => c.id === choreId);
        if (chore != undefined) {
            let choreIndex = this._lstChores.indexOf(chore);
            this._lstChores.splice(choreIndex, 1);
        }
    }

    RemoveMemberFromChore(chore: Chore, member: Member): void {
        let choreWithSameId: Chore =  this._lstChores.find(c => c.id === chore.id);
        if (choreWithSameId != undefined) {
            let choreIndex = this._lstChores.indexOf(chore);
            this._lstChores.splice(choreIndex, 1);

            chore.RemoveMember(member);
            this._lstChores.push(chore);
        }
    }
}
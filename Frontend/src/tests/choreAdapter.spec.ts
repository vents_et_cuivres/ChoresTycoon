import FakeChoreAdapter  from "../doubles/fakeChoreAdapter";
import Chore from "../models/chore";
import Member from "../models/member";
import ChorePort from "../ports/chorePort";

const adapter: ChorePort = new FakeChoreAdapter();
let member: Member = new Member("id", "name", "surname", "email");
let chore: Chore;

beforeEach(() => {
  chore = adapter.CreateChore("Default Chore", "Description", "01/01/2099 12:00:00");
});

test('Read a created chore', () => {
  let storedChore: Chore|undefined = adapter.GetChore(chore.id);

  expect(storedChore).toBe(chore);
});

test('Complete a created chore', () => {
  adapter.CompleteChore(chore.id);

  let storedChore: Chore|undefined = adapter.GetChore(chore.id);

  expect(storedChore.completed).toBe(true);
});

test('Edit a created chore', () => {
  chore.name = "Take the dog out";
  chore.description = "";
  chore.deadline = "31/12/2099 10:00:00";

  adapter.ModifyChore(chore);

  let storedChore: Chore|undefined = adapter.GetChore(chore.id);

  expect(storedChore.name).toBe("Take the dog out");
  expect(storedChore.description).toBe("");
  expect(storedChore.deadline).toBe("31/12/2099 10:00:00");
});

test('Assign a member to a created chore', () => {
  adapter.AssignMemberToChore(chore, member);

  let storedChore: Chore|undefined = adapter.GetChore(chore.id);
  expect(storedChore.GetAssignedMembers().length).toBe(1);
  expect(storedChore.GetAssignedMembers()[0]).toBe(member);
});

test('Delete a created chore', () => {
  adapter.DeleteChore(chore.id);

  expect(adapter.GetChore(chore.id)).toBeUndefined();
});

test('Remove assigned member from chore', () => {
  adapter.AssignMemberToChore(chore, member);

  adapter.RemoveMemberFromChore(chore, member);

  let storedChore: Chore|undefined = adapter.GetChore(chore.id);
  expect(storedChore.GetAssignedMembers().length).toBe(0);
});


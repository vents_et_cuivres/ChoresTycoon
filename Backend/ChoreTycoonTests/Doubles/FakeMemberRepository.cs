﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonTests.Doubles
{
    public class FakeMemberRepository : IMemberRepository
    {
        public FakeMemberRepository()
        {
        }

        List<Member> members { get; set; } = new();

        public void Create(Member member)
        {
            members.Add((Member)member.Clone());
        }

        public Member? Get(Guid memberId)
        {
            Member retrievedMember;
            try
            {
                retrievedMember = members.First(m => m.Id == memberId);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            return (Member)retrievedMember.Clone();
        }

        public IReadOnlyCollection<Member> GetAll()
        {
            return new List<Member>(members);
        }

        public void Update(Member member)
        {
            Member retrievedMember;
            try
            {
                retrievedMember = (Member)members.First(m => m.Id == member.Id);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            members.Remove(retrievedMember);
            members.Add((Member)member.Clone());
        }

        public void Delete(Guid memberId)
        {
            Member retrievedHome;
            try
            {
                retrievedHome = (Member)members.First(m => m.Id == memberId);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            members.Remove(retrievedHome);
        }

        public Collection<Member> GetAllMembersFromHome(Home home)
        {
            return home.GetMembers();
        }

        public Collection<Member> GetAllMembersAssignedToChore(Chore chore)
        {
            return chore.GetAssignedMembers();
        }

        public void AssignMemberToChore(Guid choreId, Guid memberId)
        {

        }

        public void RemoveMemberFromChore(Guid choreId, Guid memberId)
        {

        }
    }
}

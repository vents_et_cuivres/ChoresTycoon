﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonTests.Doubles
{
    public class FakeReviewRepository : IReviewRepository
    {
        private List<Review> reviews { get; set; } = new();

        public FakeReviewRepository()
        {

        }

        public void Create(Review review)
        {
            reviews.Add((Review)review.Clone());
        }

        public void Delete(Guid reviewId)
        {
            Review retrievedReview;
            try
            {
                retrievedReview = reviews.First(r => r.Id == reviewId);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            reviews.Remove(retrievedReview);
        }

        public Review? Get(Guid reviewId)
        {
            Review retrievedReview;
            try
            {
                retrievedReview = (Review)reviews.First(r => r.Id == reviewId);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            return (Review)retrievedReview.Clone();
        }

        public IReadOnlyCollection<Review> GetAll()
        {
            return new List<Review>(reviews);
        }

        public void Update(Review review)
        {
            Review retrievedReview;
            try
            {
                retrievedReview = reviews.First(r => r.Id == review.Id);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            reviews.Remove(retrievedReview);
            reviews.Add((Review)review.Clone());
        }

        public Collection<Review> GetAllReviewsFromChore(Chore chore)
        {
            return chore.GetReviews();
        }
    }
}

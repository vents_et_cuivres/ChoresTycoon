﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ChoresTycoonTests.Doubles
{
    internal class FakeChoreRepository : IChoreRepository
    {
        public FakeChoreRepository()
        {
        }

        List<Chore> chores { get; set; } = new();

        public void Create(Chore chore)
        {
            chores.Add((Chore)chore.Clone());  
        }

        public Chore? Get(Guid choreId)
        {
            Chore retrievedChore;
            try
            {
                retrievedChore = chores.First(c => c.Id == choreId);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            return (Chore)retrievedChore.Clone();
        }

        public IReadOnlyCollection<Chore> GetAll()
        {
            return new List<Chore>(chores);  
        }

        public void Update(Chore chore)
        {
            Chore retrievedChore;
            try
            {
                retrievedChore = chores.First(c => c.Id == chore.Id);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            chores.Remove(retrievedChore);
            chores.Add((Chore)chore.Clone());
        }

        public void Delete(Guid choreId)
        {
            Chore retrievedChore;
            try
            {
                retrievedChore = chores.First(c => c.Id == choreId);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            chores.Remove(retrievedChore);
        }

        public Collection<Chore> GetAllChoresFromHome(Home home)
        {
            return home.GetChores();
        }

        public Collection<Chore> GetAllChoresFromMember(Guid memberId)
        {
            throw new NotImplementedException();
        }
    }
}
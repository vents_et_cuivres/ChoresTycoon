﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonTests.Doubles
{
    public class FakeAccountRepository : IAccountRepository
    {
        List<Account> accounts { get; set; } = new();

        public void Create(Account account)
        {
            accounts.Add((Account)account.Clone());
        }

        public Account? Get(Guid accountId)
        {
            Account retrievedAccount;
            try
            {
                retrievedAccount = accounts.First(a => a.Id == accountId);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            return (Account)retrievedAccount.Clone();
        }

        public IReadOnlyCollection<Account> GetAll()
        {
            return new List<Account>(accounts);
        }

        public void Update(Account account)
        {
            Account foundAccount;
            try
            {
                foundAccount = accounts.First(a => a.Id == account.Id);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            accounts.Remove(foundAccount);
            accounts.Add((Account)account.Clone());
        }

        public void Delete(Guid accountId)
        {
            Account foundAccount;
            try
            {
                foundAccount = accounts.First(a => a.Id == accountId);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            accounts.Remove(foundAccount);
        }

        public Account? GetAccountByEmail(string email)
        {
            Account foundAccount;
            try
            {
                foundAccount = accounts.First(a => a.Email == email);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            return foundAccount;
        }
    }
}

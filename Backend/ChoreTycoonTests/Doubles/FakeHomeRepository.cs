﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonTests.Doubles
{
    internal class FakeHomeRepository : IHomeRepository
    {

        public FakeHomeRepository()
        {
        }

        List<Home> homes { get; set; } = new();

        public void Create(Home home)
        {
            homes.Add((Home)home.Clone());
        }

        public void Delete(Guid homeId)
        {
            Home retrievedHome;
            try
            {
                retrievedHome = homes.First(c => c.Id == homeId);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            homes.Remove(retrievedHome);
        }

        public Home? Get(Guid homeId)
        {
            Home home;
            try
            {
                home = homes.First(c => c.Id == homeId);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            return (Home)home.Clone();
        }

        public IReadOnlyCollection<Home> GetAll()
        {
            return homes;
        }

        public void Update(Home home)
        {
            Home retrievedHome;
            try
            {
                retrievedHome = homes.First(c => c.Id == home.Id);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            homes.Remove(retrievedHome);
            homes.Add((Home)home.Clone());
        }

        public void AddMemberToHome(Home home, Member member)
        {
            Home? storedHome;
            try
            {
                storedHome = homes.First(c => c.Id == home.Id);
            }
            catch (InvalidOperationException)
            {
                return;
            }

            Member? memberWithSameIdInHome;
            try
            {
                memberWithSameIdInHome = storedHome.GetMembers().First(m => m.Id == member.Id);
            }
            catch (InvalidOperationException)
            {
                home.AddMember((Member)member.Clone());
                Update(home);
            }
        }

        public void RemoveMemberFromHome(Home home, Member member)
        {
            Home? storedHome;
            try
            {
                storedHome = homes.First(c => c.Id == home.Id);
            }
            catch (InvalidOperationException)
            {
                return;
            }

            Member? memberWithSameIdInHome;
            try
            {
                memberWithSameIdInHome = storedHome.GetMembers().First(m => m.Id == member.Id);
            }
            catch (InvalidOperationException)
            {
                return;
            }

            home.RemoveMember(member);
            Update(home);
        }

        public bool MemberBelongsToHome(Guid homeId, Guid memberId)
        {
            bool memberBelongsToHome = false;

            Home? home = Get(homeId);

            if (home == null)
                return false;

            foreach (Member member in home.GetMembers())
            {
                if (member.Id == memberId)
                    memberBelongsToHome = true;
            }

            return memberBelongsToHome;
        }

        public void DeleteHomeMembers(Guid id)
        {
            //
        }

        public Collection<Home> GetAllHomesFromMember(Guid memberId)
        {
            throw new NotImplementedException();
        }
    }
}

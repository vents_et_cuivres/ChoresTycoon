﻿//using ChoresTycoonCore.DataTypes;
//using ChoresTycoonCore.Entities;
//using ChoresTycoonCore.Exceptions;
//using System;
//using System.Collections.Generic;
//using Xunit;

//namespace ChoresTycoonTests.Entities
//{
//    public class HomeTestsDataSet
//    {
//        public static IEnumerable<object[]> MembersData
//        {
//            get
//            {
//                yield return new object[] { new Member("Martin", "Hoffmann")};
//                yield return new object[] { new Member("Maxime", "Eckstein") };
//                yield return new object[] { new Member("Alexandre", "Colombo") };
//                yield return new object[] { new Member("Claude", "Schlumberger") };
//            }
//        }
//    }
//    public class HomeTests
//    {
//        [Theory]
//        [InlineData("")]
//        [InlineData("          ")]
//        public void EmptyNameThrowsException(string homeName)
//        {
//            Assert.Throws<InvalidHomeNameException>(() => new HomeName(homeName));
//        }

//        [Theory]
//        [MemberData(nameof(HomeTestsDataSet.MembersData), MemberType = typeof(HomeTestsDataSet))]
//        public void AddMemberToHome(Member member)
//        {
//            var home = new Home();
//            home.AddMember(member);
//            Assert.Contains<Member>(member, home.GetMembers());
//        }

//        [Theory]
//        [MemberData(nameof(HomeTestsDataSet.MembersData), MemberType = typeof(HomeTestsDataSet))]
//        public void DuplicateMemberInHomeThrowsException(Member member)
//        {
//            var home = new Home();
//            home.AddMember(member);
//            Assert.Throws<DuplicateMemberException>(() => home.AddMember(member));
//        }

//        [Theory]
//        [MemberData(nameof(HomeTestsDataSet.MembersData), MemberType = typeof(HomeTestsDataSet))]
//        public void RemoveMemberFromHome(Member member)
//        {
//            var home = new Home();
//            home.AddMember(member);
//            home.RemoveMember(member);
//            Assert.DoesNotContain<Member>(member, home.GetMembers());
//        }

//        [Theory]
//        [MemberData(nameof(HomeTestsDataSet.MembersData), MemberType = typeof(HomeTestsDataSet))]
//        public void RemoveInexistentMemberThrowsException(Member member)
//        {
//            var home = new Home();
//            Assert.Throws<RemoveInexistentMember>(() => home.RemoveMember(member));
//        }
//    }
//}

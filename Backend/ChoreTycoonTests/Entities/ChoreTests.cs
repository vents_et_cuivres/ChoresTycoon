﻿//using ChoresTycoonCore.Entities;
//using ChoresTycoonCore.Enums;
//using ChoresTycoonCore.Exceptions;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using Xunit;

//namespace ChoresTycoonTests.Entities
//{
//    public class ChoreTestsDataSet
//    {
//        public static IEnumerable<object[]> MembersData
//        {
//            get
//            {
//                yield return new object[] { 3, new Collection<Member>() { 
//                    new Member("Martin", "Hoffmann"), 
//                    new Member("Maxime", "Eckstein"),
//                    new Member("Alexandre", "Colombo") } 
//                };
//            }
//        }

//        public static IEnumerable<object[]> ChoreReviewData
//        {
//            get
//            {
//                yield return new object[] { new Review(new Member("Martin", "Hoffmann"), EChoreReviewScore.Done) };
//                yield return new object[] { new Review(new Member("Maxime", "Eckstein"), EChoreReviewScore.NotDone) };
//                yield return new object[] { new Review(new Member("Alexandre", "Colombo"), EChoreReviewScore.WellDone) };
//                yield return new object[] { new Review(new Member("Claude", "Schlumberger"), EChoreReviewScore.BadlyDone) };
//            }
//        }

//        public static IEnumerable<object[]> DatesBefore2000
//        {
//            get
//            {
//                yield return new object[] { new DateTime(1999, 12, 31, 23, 59, 59, 999) };
//                yield return new object[] { new DateTime(1654, 03, 12) };
//            }
//        }

//    }

//    public class ChoreTests
//    {
//        [Theory]
//        [MemberData(nameof(ChoreTestsDataSet.MembersData), MemberType = typeof(ChoreTestsDataSet))]
//        public void AssignMembersToChore(int expectedMembersCount, Collection<Member> members)
//        {
//            var chore = new Chore("TestChore", "Description", DateTime.Now, new Guid());
//            chore.AssignMembers(members);
//            Assert.Equal(expectedMembersCount, chore.GetAssignedMembers().Count);
//        }

//        [Fact]
//        public void CompleteChore()
//        {
//            var chore = new Chore("TestChore", "Description", DateTime.Now, new Guid());
//            chore.Complete();
//            Assert.True(chore.Completed);
//        }

//        [Theory]
//        [MemberData(nameof(ChoreTestsDataSet.ChoreReviewData), MemberType = typeof(ChoreTestsDataSet))]
//        public void ReviewCompletedChore(Review review)
//        {
//            var chore = new Chore("TestChore", "Description", DateTime.Now, new Guid());
//            chore.Complete();
//            chore.AddReview(review);
//            Assert.Contains<Review>(review, chore.GetReviews());
//        }

//        [Theory]
//        [InlineData("")]
//        [InlineData("   ")]
//        [InlineData(null)]
//        public void ChoreNameCannotBeEmpty(string choreName)
//        {
//            Exception e = Assert.Throws<InvalidChoreNameException>(() => new Chore(choreName, "Description", DateTime.Now, new Guid()));
//            Assert.Equal("Chore name cannot be empty.", e.Message);
//        }

//        [Theory]
//        [MemberData(nameof(ChoreTestsDataSet.DatesBefore2000), MemberType = typeof(ChoreTestsDataSet))]
//        public void ChoreDeadlineCannotBeBefore2000(DateTime deadline)
//        {
//            Assert.Throws<InvalidChoreDeadlineException>(() => new Chore("Name", "Description", deadline, new Guid()));
//        }
//    }
//}

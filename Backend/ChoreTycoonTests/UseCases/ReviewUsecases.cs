﻿using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Enums;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonTests.Doubles;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ChoresTycoonTests.UseCases
{
    public class ReviewUsecases
    {
        private IReviewRepository reviewRepository { get; set; }
        private IMemberRepository memberRepository { get; set; }
        private IChoreRepository choreRepository { get; set; }
        private IHomeRepository homeRepository { get; set; }

        private Review reviewOnCompletedChore;
        private Review reviewOnUncompletedChore;
        private Member member;
        private Chore completedChore;
        private Chore uncompletedChore;
        private Home home;

        private Home otherHome;
        private Member otherMember;
        private Review reviewOnCompletedChoreByMemberFromOtherHome;

        public ReviewUsecases()
        {
            reviewRepository = new MysqlReviewRepository();
            memberRepository = new MysqlMemberRepository();
            choreRepository = new MysqlChoreRepository();
            homeRepository = new MysqlHomeRepository();

            string timestamp = DateTime.Now.Ticks.ToString();

            home = new Home(timestamp);
            otherHome = new Home(timestamp);
            var createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);
            createHomeUsecase.Execute(otherHome);

            member = new Member(timestamp, timestamp);
            otherMember = new Member(timestamp, timestamp);
            var createMemberUsecase = new CreateMember(memberRepository);
            createMemberUsecase.Execute(member);
            createMemberUsecase.Execute(otherMember);

            var addMemberToHomeUsecase = new AddMemberToHome(homeRepository);
            addMemberToHomeUsecase.Execute(home, member);
            addMemberToHomeUsecase.Execute(otherHome, otherMember);

            completedChore = new Chore($"Completed-{timestamp}", "Completed", DateTime.Now, home.Id);
            completedChore.Complete();

            uncompletedChore = new Chore($"Uncompleted-{timestamp}", "Uncompleted", DateTime.Now, home.Id);
            CreateChore createChoreUsecase = new CreateChore(choreRepository, homeRepository);
            createChoreUsecase.Execute(home, completedChore);
            createChoreUsecase.Execute(home, uncompletedChore);

            reviewOnCompletedChore = new Review(member, completedChore, EChoreReviewScore.Done);
            reviewOnUncompletedChore = new Review(member, uncompletedChore, EChoreReviewScore.Done);
            reviewOnCompletedChoreByMemberFromOtherHome = new Review(otherMember, completedChore, EChoreReviewScore.Done);
        }

        [Fact]
        public void CreatedReviewIsSaved()
        {
            var createReviewUsecase = new CreateReview(reviewRepository, choreRepository, homeRepository);
            createReviewUsecase.Execute(completedChore, reviewOnCompletedChore);

            var storedReview = reviewRepository.Get(reviewOnCompletedChore.Id);

            Assert.Equal(reviewOnCompletedChore.Id, storedReview.Id);
            Assert.Equal(reviewOnCompletedChore.ReviewScore, storedReview.ReviewScore);
            Assert.Equal(reviewOnCompletedChore.ChoreId, storedReview.ChoreId);
            Assert.Equal(reviewOnCompletedChore.MemberId, storedReview.MemberId);
        }

        [Fact]
        public void CreatedReviewCanBeModified()
        {
            var createReviewUsecase = new CreateReview(reviewRepository, choreRepository, homeRepository);
            createReviewUsecase.Execute(completedChore, reviewOnCompletedChore);

            EChoreReviewScore newScore = EChoreReviewScore.BadlyDone;
            reviewOnCompletedChore.ReviewScore = newScore;

            var modifyReviewUsecase = new ModifyReview(reviewRepository);
            modifyReviewUsecase.Execute(reviewOnCompletedChore);

            var storedReview = reviewRepository.Get(reviewOnCompletedChore.Id);
            Assert.Equal(reviewOnCompletedChore.ReviewScore, storedReview.ReviewScore);
        }

        [Fact]
        public void CreatedReviewCanBeDeleted()
        {
            var createReviewUsecase = new CreateReview(reviewRepository, choreRepository, homeRepository);
            createReviewUsecase.Execute(completedChore, reviewOnCompletedChore);

            var deleteReviewUsecase = new DeleteReview(reviewRepository);
            deleteReviewUsecase.Execute(reviewOnCompletedChore.Id);

            Assert.Null(reviewRepository.Get(reviewOnCompletedChore.Id));
        }

        [Fact]
        public void UncompletedChoreCannotBeReviewed()
        {
            var createReviewUsecase = new CreateReview(reviewRepository, choreRepository, homeRepository);
            Assert.Throws<Exception>(() => createReviewUsecase.Execute(uncompletedChore, reviewOnUncompletedChore));
        }

        [Fact]
        public void MemberCannotReviewChoreWhenNotInSameHome()
        {
            var createReviewUsecase = new CreateReview(reviewRepository, choreRepository, homeRepository);
            Assert.Throws<Exception>(() => createReviewUsecase.Execute(completedChore, reviewOnCompletedChoreByMemberFromOtherHome));
        }

        [Fact]
        public void MemberCannotReviewSameChoreTwice()
        {
            var createReviewUsecase = new CreateReview(reviewRepository, choreRepository, homeRepository);
            createReviewUsecase.Execute(completedChore, reviewOnCompletedChore);
            createReviewUsecase.Execute(completedChore, reviewOnCompletedChore);

            Collection<Review> choreReviews = reviewRepository.GetAllReviewsFromChore(completedChore);


            int numberOfReviewBySameMember = 0;
            foreach (Review review in choreReviews)
            {
                if (review.MemberId == reviewOnCompletedChore.MemberId)
                    numberOfReviewBySameMember++;
            }
            Assert.Equal(1, numberOfReviewBySameMember);
        }
    }
}

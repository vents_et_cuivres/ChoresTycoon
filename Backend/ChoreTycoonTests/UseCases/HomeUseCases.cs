﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using ChoresTycoonTests.Doubles;
using ChoresTycoonCore.Adapters;

namespace ChoresTycoonTests.UseCases
{
    public  class HomeUseCases
    {
        private Home home;
        private Home home2;
        private Member member;
        private Member member2;
        private Chore chore;
        private Chore chore2;
        private readonly IHomeRepository homeRepository;
        private readonly IMemberRepository memberRepository;
        private readonly IChoreRepository choreRepository;

        public HomeUseCases()
        {
            homeRepository = new MysqlHomeRepository();
            choreRepository = new MysqlChoreRepository();
            memberRepository = new MysqlMemberRepository();

            home = new Home("TestHome");
            home2 = new Home("TestHome2");

            member = new Member("surname", "name");
            member2 = new Member("surname2", "name2");
            var createMemberUsecase = new CreateMember(memberRepository);
            createMemberUsecase.Execute(member);
            createMemberUsecase.Execute(member2);

            chore = new Chore("TestChore", "Description", DateTime.Now, home.Id);
            chore2 = new Chore("TestChore2", "Description2", DateTime.Now, home.Id);
        }

        [Fact]
        public void CreatedHomeIsSaved()
        {
            CreateHome createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            var storedHome = homeRepository.Get(home.Id);

            Assert.Equal(home.Id, storedHome.Id);
            Assert.Equal(home.Name, storedHome.Name);
        }

        [Fact]
        public void CreatedHomeCanBeDeleted()
        {
            CreateHome createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            DeleteHome deleteHomeUsecase = new DeleteHome(homeRepository);
            deleteHomeUsecase.Execute(home);

            Assert.Null(homeRepository.Get(home.Id));
        }

        [Fact]
        public void CreatedHomeCanBeModified()
        {
            CreateHome createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);
            
            string newName = "NewName";
            home.Name = newName;

            ModifyHome usecase = new ModifyHome(homeRepository);
            usecase.Execute(home);

            var storedHome = homeRepository.Get(home.Id);

            Assert.Equal(storedHome.Name, newName);
        }

        [Fact]
        public void MemberCanBeAddedToCreatedHome()
        {
            var createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            var addMemberUsecase = new AddMemberToHome(homeRepository);
            addMemberUsecase.Execute(home, member);

            var retrieveHomeUsecase = new RetrieveHomeWithChoresAndMembers(homeRepository, memberRepository, choreRepository);
            var storedHome = retrieveHomeUsecase.Execute(home.Id);

            Assert.NotNull(storedHome.GetMembers().First(m => m.Id == member.Id));
        }

        [Fact]
        public void MemberCanBeRemovedFromCreatedHome()
        {
            var createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            var addMemberUsecase = new AddMemberToHome(homeRepository);
            addMemberUsecase.Execute(home, member);

            var removeMemberFromHomeUsecase = new RemoveMemberFromHome(homeRepository);
            removeMemberFromHomeUsecase.Execute(home, member);

            var retrieveHomeUsecase = new RetrieveHomeWithChoresAndMembers(homeRepository, memberRepository, choreRepository);
            var storedHome = retrieveHomeUsecase.Execute(home.Id);

            Assert.Null(storedHome.GetMembers().FirstOrDefault(m => m.Id == member.Id, null));
        }

        [Fact]
        public void GetMembersFromHome()
        {
            var createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            var addMemberUsecase = new AddMemberToHome(homeRepository);
            addMemberUsecase.Execute(home, member);
            addMemberUsecase.Execute(home, member2);

            var retrieveHomeUsecase = new RetrieveHomeWithChoresAndMembers(homeRepository, memberRepository, choreRepository);
            var storedHome = retrieveHomeUsecase.Execute(home.Id);

            var collectionOfMembersFromHome = storedHome.GetMembers();
            
            Assert.NotNull(collectionOfMembersFromHome.FirstOrDefault(m => m.Id == member.Id, null));
            Assert.NotNull(collectionOfMembersFromHome.FirstOrDefault(m => m.Id == member2.Id, null));
            Assert.Equal(2, collectionOfMembersFromHome.Count);
        }

        [Fact]
        public void AddChoreToCreatedHome()
        {
            CreateHome createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            CreateChore createChoreUsecase = new CreateChore(choreRepository, homeRepository);
            createChoreUsecase.Execute(home, chore);

            var retrieveHomeUsecase = new RetrieveHomeWithChoresAndMembers(homeRepository, memberRepository, choreRepository);
            var storedHome = retrieveHomeUsecase.Execute(home.Id);

            Assert.NotNull(storedHome.GetChores().FirstOrDefault(c => c.Id == chore.Id, null));
        }

        [Fact]
        public void RemoveChoreFromCreatedHome()
        {
            var createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            var addChoreUsecase = new AddChoreToHome(homeRepository);
            addChoreUsecase.Execute(home, chore);

            var removeChoreFromHomeUsecase = new RemoveChoreFromHome(homeRepository);
            removeChoreFromHomeUsecase.Execute(home, chore);

            var retrieveHomeUsecase = new RetrieveHomeWithChoresAndMembers(homeRepository, memberRepository, choreRepository);
            var storedHome = retrieveHomeUsecase.Execute(home.Id);

            Assert.Null(storedHome.GetChores().FirstOrDefault(c => c.Id == chore.Id, null));
        }

        [Fact]
        public void GetChoresFromHome()
        {
            var createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            CreateChore createChoreUsecase = new CreateChore(choreRepository, homeRepository);
            createChoreUsecase.Execute(home, chore);
            createChoreUsecase.Execute(home, chore2);

            var retrieveHomeUsecase = new RetrieveHomeWithChoresAndMembers(homeRepository, memberRepository, choreRepository);
            var storedHome = retrieveHomeUsecase.Execute(home.Id);

            var collectionOfChoresFromHome = storedHome.GetChores();

            Assert.NotNull(collectionOfChoresFromHome.FirstOrDefault(c => c.Id == chore.Id, null));
            Assert.NotNull(collectionOfChoresFromHome.FirstOrDefault(c => c.Id == chore2.Id, null));
            Assert.Equal(2, collectionOfChoresFromHome.Count);
        }

        [Fact]
        public void RetrievedHomeContainsAllHomeMembers()
        {
            var createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            var addMemberUsecase = new AddMemberToHome(homeRepository);
            addMemberUsecase.Execute(home, member);
            addMemberUsecase.Execute(home, member2);

            var modifyHomeUsecase = new ModifyHome(homeRepository);
            modifyHomeUsecase.Execute(home);

            var retrieveHomeUsecase = new RetrieveHomeWithChoresAndMembers(homeRepository, memberRepository, choreRepository);
            Home retrievedHome = retrieveHomeUsecase.Execute(home.Id);

            Member firstMember = retrievedHome.GetMembers().First(m => m.Id == member.Id);
            Assert.Equal(member.Id, firstMember.Id);
            Assert.Equal(member.Surname, firstMember.Surname);
            Assert.Equal(member.Name, firstMember.Name);

            Member secondMember = retrievedHome.GetMembers().First(m => m.Id == member2.Id);
            Assert.Equal(member2.Id, secondMember.Id);
            Assert.Equal(member2.Surname, secondMember.Surname);
            Assert.Equal(member2.Name, secondMember.Name);
        }

        [Fact]
        public void RetrievedHomeContainsAllHomeChores()
        {
            var createHomeUsecase = new CreateHome(homeRepository);
            createHomeUsecase.Execute(home);

            CreateChore createChoreUsecase = new CreateChore(choreRepository, homeRepository);
            createChoreUsecase.Execute(home, chore);
            createChoreUsecase.Execute(home, chore2);

            var addChoreUsecase = new AddChoreToHome(homeRepository);
            addChoreUsecase.Execute(home, chore);
            addChoreUsecase.Execute(home, chore2);

            var modifyHomeUsecase = new ModifyHome(homeRepository);
            modifyHomeUsecase.Execute(home);

            var retrieveHomeUsecase = new RetrieveHomeWithChoresAndMembers(homeRepository, memberRepository, choreRepository);
            Home retrievedHome = retrieveHomeUsecase.Execute(home.Id);

            Chore firstChore = retrievedHome.GetChores().First(c => c.Id == chore.Id);
            Assert.Equal(chore.Id, firstChore.Id);
            Assert.Equal(chore.Name, firstChore.Name);
            Assert.Equal(chore.Description, firstChore.Description);
            Assert.Equal(chore.Deadline, firstChore.Deadline);

            Chore secondChore = retrievedHome.GetChores().First(c => c.Id == chore2.Id);
            Assert.Equal(chore2.Id, secondChore.Id);
            Assert.Equal(chore2.Name, secondChore.Name);
            Assert.Equal(chore2.Description, secondChore.Description);
            Assert.Equal(chore2.Deadline, secondChore.Deadline);
        }

    }


}

﻿using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonTests.Doubles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ChoresTycoonTests.UseCases
{
    public class AccountUsecases
    {
        private IAccountRepository accountRepository { get; set; }
        private IMemberRepository memberRepository { get; set; }
        private Account account { get; set; }
        private Account otherAccount { get; set; }

        public AccountUsecases()
        {
            accountRepository = new MysqlAccountRepository();
            memberRepository = new MysqlMemberRepository();

            string firstTimestamp = DateTime.Now.Ticks.ToString();
            account = new Account($"{firstTimestamp}@mail.com", "secret");

            string secondTimestamp = DateTime.Now.Ticks.ToString();
            otherAccount = new Account($"{secondTimestamp}@mail.com", "notSecret");
        }

        [Fact]
        public void CreatedAccountIsSaved()
        {
            var accountCreationUsecase = new CreateAccount(accountRepository, memberRepository);
            accountCreationUsecase.Execute(account);

            var storedAccount = accountRepository.Get(account.Id);

            Assert.Equal(account.Id, storedAccount.Id);
            Assert.Equal(account.Email, storedAccount.Email);
            Assert.Equal(account.Password, storedAccount.Password);
        }

        [Fact]
        public void CannotCreateMultipleAccountWithSameEmail()
        {
            var accountCreationUsecase = new CreateAccount(accountRepository, memberRepository);
            accountCreationUsecase.Execute(account);

            Assert.Throws<Exception>(() => accountCreationUsecase.Execute(account));
        }

        [Fact]
        public void NewMemberIsCreatedOnAccountCreation()
        {
            var accountCreationUsecase = new CreateAccount(accountRepository, memberRepository);
            accountCreationUsecase.Execute(account);

            var storedAccount = accountRepository.Get(account.Id);
            var associatedMember = memberRepository.Get(account.MemberId);

            Assert.Equal("Doe", associatedMember.Name);
            Assert.Equal("John", associatedMember.Surname);
        }

        [Fact]
        public void CreatedAccountCanBeModified()
        {
            var accountCreationUsecase = new CreateAccount(accountRepository, memberRepository);
            accountCreationUsecase.Execute(account);

            string timestamp = DateTime.Now.Ticks.ToString();
            string newMail = $"{timestamp}@mail.com";
            string newPassword = "newSecret";

            account.Email = newMail;
            account.Password = newPassword;

            var accountModifyUsecase = new ModifyAccount(accountRepository);
            accountModifyUsecase.Execute(account);

            var storedAccount = accountRepository.Get(account.Id);

            Assert.Equal(newMail, storedAccount.Email);
            Assert.Equal(newPassword, storedAccount.Password);
        }

        [Fact]
        public void CannotSetAccountEmailWithAlreadyUsedEmail()
        {
            var accountCreationUsecase = new CreateAccount(accountRepository, memberRepository);
            accountCreationUsecase.Execute(account);
            accountCreationUsecase.Execute(otherAccount);

            account.Email = otherAccount.Email;
            var accountModifyUsecase = new ModifyAccount(accountRepository);
            Assert.Throws<Exception>(() => accountModifyUsecase.Execute(account));
        }

        [Fact]
        public void CreatedAccountCanBeDeleted()
        {
            var accountCreationUsecase = new CreateAccount(accountRepository, memberRepository);
            accountCreationUsecase.Execute(account);

            var accountDeleteUsecase = new DeleteAccount(accountRepository);
            accountDeleteUsecase.Execute(account);

            Assert.Null(accountRepository.Get(account.Id));
        }

        [Fact]
        public void CanLoginWithExistingAccount()
        {
            var accountCreationUsecase = new CreateAccount(accountRepository, memberRepository);
            accountCreationUsecase.Execute(account);

            var accountLoginUsecase = new LoginAccount(accountRepository);
            Assert.True(accountLoginUsecase.Execute(account.Email, account.Password));
        }

        [Fact]
        public void CannotLoginWithWrongPassword()
        {
            var accountCreationUsecase = new CreateAccount(accountRepository, memberRepository);
            accountCreationUsecase.Execute(account);

            var accountLoginUsecase = new LoginAccount(accountRepository);
            Assert.False(accountLoginUsecase.Execute(account.Email, otherAccount.Password));
        }

        [Fact]
        public void CannotLoginWithInexistentAccount()
        {
            var accountLoginUsecase = new LoginAccount(accountRepository);
            Assert.False(accountLoginUsecase.Execute("notAnAccount@mail.com", "password"));
        }
    }
}

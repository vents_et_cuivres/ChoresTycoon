using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.DataTypes;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Enums;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonTests.Doubles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ChoresTycoonTests.UseCases
{
    public class ChoresUseCases
    {
        private readonly IChoreRepository choreRepository;
        private readonly IHomeRepository homeRepository;
        private readonly IMemberRepository memberRepository;
        private readonly IReviewRepository reviewRepository;
        private Chore chore;
        private Member member;
        private Member member2;
        private Member member3;
        private Home home;
        private Review review;
        private Review review2;
        private Review review3;

        public ChoresUseCases()
        {
            string timestamp = DateTime.Now.Ticks.ToString();

            choreRepository = new MysqlChoreRepository();
            homeRepository = new MysqlHomeRepository();
            memberRepository = new MysqlMemberRepository();
            reviewRepository = new MysqlReviewRepository();

            home = new Home(timestamp);
            homeRepository.Create(home);

            member = new Member("Surname", "Name");
            member2 = new Member("Surname2", "Name2");
            member3 = new Member("Surname3", "Name3");
            memberRepository.Create(member);
            memberRepository.Create(member2);
            memberRepository.Create(member3);

            var addMemberToHomeUsecase = new AddMemberToHome(homeRepository);
            addMemberToHomeUsecase.Execute(home, member);
            addMemberToHomeUsecase.Execute(home, member2);
            addMemberToHomeUsecase.Execute(home, member3);

            chore = new Chore("TestChore", "Description", DateTime.Now, home.Id);
            review = new Review(member, chore, EChoreReviewScore.WellDone);
            review2 = new Review(member2, chore, EChoreReviewScore.BadlyDone);
            review3 = new Review(member3, chore, EChoreReviewScore.BadlyDone);
        }

        [Fact]
        public void CreatedChoreIsSaved()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            var storedChore = choreRepository.Get(chore.Id);

            Assert.Equal(chore.Id, storedChore.Id);
            Assert.Equal(chore.Name, storedChore.Name);
            Assert.Equal(chore.Description, storedChore.Description);
            Assert.Equal(chore.Deadline, storedChore.Deadline);
            Assert.Equal(chore.HomeId, storedChore.HomeId);
        }

        [Fact]
        public void CreatedChoreCanBeDeleted()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            var deleteChoreUsecase = new DeleteChore(choreRepository);
            deleteChoreUsecase.Execute(chore);

            Assert.Null(choreRepository.Get(chore.Id));
        }

        [Fact]
        public void CreatedChoreCanBeModified()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            string newName = "Different name";
            string newDescription = "Different Description";
            ChoreDeadline newDeadline = DateTime.Now.AddDays(10);

            chore.Name = newName;
            chore.Description = newDescription;
            chore.Deadline = newDeadline;

            var modifyChoreUsecase = new ModifyChore(choreRepository);
            modifyChoreUsecase.Execute(chore);

            var storedChore = choreRepository.Get(chore.Id);

            Assert.Equal(storedChore.Name, newName);
            Assert.Equal(storedChore.Description, newDescription);
            Assert.Equal((DateTime)storedChore.Deadline, (DateTime)newDeadline);
            Assert.Equal(storedChore.HomeId, chore.HomeId);
        }

        [Fact]
        public void CreatedChoreCanBeCompleted()
        {
            CreateChore createChoreUsecase = new CreateChore(choreRepository, homeRepository);
            createChoreUsecase.Execute(home, chore);

            CompleteChore completeChoreUsecase = new CompleteChore(choreRepository);
            completeChoreUsecase.Execute(chore);
            var newChore = choreRepository.Get(chore.Id);

            Assert.True(newChore.Completed);
        }

        [Fact]
        public void MemberCanBeAssignedToCreatedChore()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            var assignMemberUsecase = new AssignMemberToChore(choreRepository, memberRepository);
            assignMemberUsecase.Execute(chore, member);

            var retrieveWholeChoreUsecase = new RetrieveChoreWithMembersAndReviews(choreRepository, memberRepository, reviewRepository);
            Chore storedChore = retrieveWholeChoreUsecase.Execute(chore.Id);

            Assert.NotNull(storedChore.GetAssignedMembers().FirstOrDefault(m => m.Id == member.Id, null));
        }

        [Fact]
        public void MemberCanBeRemovedFromCreatedChore()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            var assignMemberUsecase = new AssignMemberToChore(choreRepository, memberRepository);
            assignMemberUsecase.Execute(chore, member);

            var removeAssignedMemberUsecase = new RemoveMemberFromChore(choreRepository, memberRepository);
            removeAssignedMemberUsecase.Execute(chore, member);

            var retrieveWholeChoreUsecase = new RetrieveChoreWithMembersAndReviews(choreRepository, memberRepository, reviewRepository);
            Chore storedChore = retrieveWholeChoreUsecase.Execute(chore.Id);

            Assert.Null(storedChore.GetAssignedMembers().FirstOrDefault(m => m.Id == member.Id, null));
        }

        [Fact]
        public void MemberCannotBeAssignedTwiceToSameChore()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            var assignMemberUsecase = new AssignMemberToChore(choreRepository, memberRepository);
            assignMemberUsecase.Execute(chore, member);
            assignMemberUsecase.Execute(chore, member);

            var retrieveWholeChoreUsecase = new RetrieveChoreWithMembersAndReviews(choreRepository, memberRepository, reviewRepository);
            Chore storedChore = retrieveWholeChoreUsecase.Execute(chore.Id);

            int numberOfTimeMemberIsAssigned = 0;
            foreach(Member assignedMember in storedChore.GetAssignedMembers())
            {
                if (assignedMember.Id == member.Id)
                    numberOfTimeMemberIsAssigned++;
            }
            Assert.Equal(1, numberOfTimeMemberIsAssigned);
        }

        [Fact]
        public void GetAverageReviewScoreFromChore()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            var completeChoreUsecase = new CompleteChore(choreRepository);
            completeChoreUsecase.Execute(chore);

            var createReviewUsecase = new CreateReview(reviewRepository, choreRepository, homeRepository);
            createReviewUsecase.Execute(chore, review);
            createReviewUsecase.Execute(chore, review2);
            createReviewUsecase.Execute(chore, review3);

            var retrieveWholeChoreUsecase = new RetrieveChoreWithMembersAndReviews(choreRepository, memberRepository, reviewRepository);
            Chore storedChore = retrieveWholeChoreUsecase.Execute(chore.Id);
            var averageReviewScore = storedChore.GetAverageReviewScore();
            
            Assert.Equal(3,storedChore.GetReviews().Count());
            Assert.Equal(1.7,averageReviewScore);
        }

        [Fact]
        public void RetrievedChoreContainsAllAssignedMembers()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            var assignMemberToChoreUsecase = new AssignMemberToChore(choreRepository, memberRepository);
            assignMemberToChoreUsecase.Execute(chore, member);
            assignMemberToChoreUsecase.Execute(chore, member2);

            var retrieveChoreUsecase = new RetrieveChoreWithMembersAndReviews(choreRepository, memberRepository, reviewRepository);
            var retrievedChore = retrieveChoreUsecase.Execute(chore.Id);

            Member firstMember = retrievedChore.GetAssignedMembers().First(m => m.Id == member.Id);
            Assert.Equal(member.Id, firstMember.Id);
            Assert.Equal(member.Name, firstMember.Name);
            Assert.Equal(member.Surname, firstMember.Surname);

            Member secondMember = retrievedChore.GetAssignedMembers().First(m => m.Id == member2.Id);
            Assert.Equal(member2.Id, secondMember.Id);
            Assert.Equal(member2.Name, secondMember.Name);
            Assert.Equal(member2.Surname, secondMember.Surname);
        }

        [Fact]
        public void RetrievedChoreContainsAllReviews()
        {
            CreateChore usecase = new CreateChore(choreRepository, homeRepository);
            usecase.Execute(home, chore);

            CompleteChore completeChoreUsecase = new CompleteChore(choreRepository);
            completeChoreUsecase.Execute(chore);

            var createReviewUsecase = new CreateReview(reviewRepository, choreRepository, homeRepository);
            createReviewUsecase.Execute(chore, review);
            createReviewUsecase.Execute(chore, review2);

            var retrieveChoreUsecase = new RetrieveChoreWithMembersAndReviews(choreRepository, memberRepository, reviewRepository);
            var retrievedChore = retrieveChoreUsecase.Execute(chore.Id);

            Review firstReview = retrievedChore.GetReviews().First(r => r.Id == review.Id);
            Assert.Equal(review.Id, firstReview.Id);
            Assert.Equal(review.MemberId, firstReview.MemberId);
            Assert.Equal(review.ChoreId, firstReview.ChoreId);

            Review secondReview = retrievedChore.GetReviews().First(r => r.Id == review2.Id);
            Assert.Equal(review2.Id, secondReview.Id);
            Assert.Equal(review2.MemberId, secondReview.MemberId);
            Assert.Equal(review2.ChoreId, secondReview.ChoreId); ;
        }
    }
}

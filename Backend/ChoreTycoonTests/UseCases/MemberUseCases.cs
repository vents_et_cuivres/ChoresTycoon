﻿using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonTests.Doubles;
using System;
using Xunit;

namespace ChoresTycoonTests.UseCases
{
    public class MemberUseCases
    {
        private IMemberRepository repository { get; set; }
        private Member member { get; set; }

        public MemberUseCases()
        {
            repository = new MysqlMemberRepository();
            member = new Member("surname", "name");
        }

        [Fact]
        public void CreatedMemberIsSaved()
        {
            var creationUsecase = new CreateMember(repository);
            creationUsecase.Execute(member);

            var storedMember = repository.Get(member.Id);
            Assert.Equal(member.Id, storedMember.Id);
            Assert.Equal(member.Surname, storedMember.Surname);
            Assert.Equal(member.Name, storedMember.Name);
        }

        [Fact]
        public void CreatedMemberCanBeModified()
        {
            var creationUsecase = new CreateMember(repository);
            creationUsecase.Execute(member);

            string newName = "Different name";
            string newSurname = "Different surname";

            member.Name = newName;
            member.Surname = newSurname;

            var modificationUsecase = new ModifyMember(repository);
            modificationUsecase.Execute(member);

            var storedMember = repository.Get(member.Id);

            Assert.Equal(newSurname, storedMember.Surname);
            Assert.Equal(newName, storedMember.Name);
        }

        [Fact]
        public void CreatedMemberCanBeDeleted()
        {
            var creationUsecase = new CreateMember(repository);
            creationUsecase.Execute(member);

            var deleteUsecase = new DeleteMember(repository);
            deleteUsecase.Execute(member);

            Assert.Null(repository.Get(member.Id));
        }
    }
}

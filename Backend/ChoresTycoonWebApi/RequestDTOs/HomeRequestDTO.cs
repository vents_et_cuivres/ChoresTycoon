﻿namespace ChoresTycoonWebApi.RequestBodies
{
    public class HomeRequestDTO
    {
        /// <summary>
        /// Description for Swagger
        /// </summary>
        /// <example>Super coloc</example>
        public string Name { get; set; } = "";
        public Guid homeId { get; set; }
        public Guid memberId { get; set; }

        public Guid choreId { get; set; }
    }
}

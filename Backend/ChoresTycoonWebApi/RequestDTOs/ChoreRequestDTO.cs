using ChoresTycoonCore.DataTypes;

namespace ChoresTycoonWebApi.RequestBodies
{
    public class ChoreRequestDTO
    {
        /// <summary>
        /// Description for Swagger
        /// </summary>
        /// <example>Super coloc</example>
        public string name { get; set; } = "";
        public bool completed { get; set; } = false;
        //public ChoreDescription description { get; set; } = new ChoreDescription("default");
        //public ChoreDeadline deadline { get; set; } = new ChoreDeadline(DateTime.Now);

        public string description { get; set; } = "default";
        public DateTime deadline { get; set; } = DateTime.Now;


        public Guid homeId { get; set; }
        public Guid memberId { get; set; }
        public string surname { get; set; } = "";
        public Guid choreId { get; set; }
        public Guid reviewId { get; set; }
    }
}

namespace ChoresTycoonWebApi.RequestBodies
{
    public class MemberRequestDTO
    {
        /// <summary>
        /// Description for Swagger
        /// </summary>
        /// <example>Super coloc</example>
        public string Name { get; set; } = "";
        public Guid homeId { get; set; }
        public Guid memberId { get; set; }
        public string Surname { get; set; } = "";
        public Guid choreId { get; set; }
    }
}

using ChoresTycoonCore.Enums;

namespace ChoresTycoonWebApi.RequestBodies
{
    public class ReviewRequestDTO
    {
        /// <summary>
        /// Description for Swagger
        /// </summary>
        /// <example>Super coloc</example>
        public Guid ReviewId { get;  set; }
        public Guid ChoreId { get;  set; }
        public Guid MemberId { get;  set; }
        public EChoreReviewScore ReviewScore { get; set; }
    }
}

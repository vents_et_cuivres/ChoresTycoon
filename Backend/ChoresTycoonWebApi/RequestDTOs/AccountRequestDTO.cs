﻿namespace ChoresTycoonWebApi.RequestBodies
{
    public class AccountRequestDTO
    {
        /// <summary>
        /// Description for Swagger
        /// </summary>
        /// <example>email@mail.com</example>
        public string? Email { get; set; } = "";

        /// <summary>
        /// Description for Swagger
        /// </summary>
        /// <example>superSecret</example>
        public string? Password { get; set; } = "";
    }
}

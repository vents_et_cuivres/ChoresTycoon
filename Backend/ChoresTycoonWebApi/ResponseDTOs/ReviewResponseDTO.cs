﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Enums;

namespace ChoresTycoonWebApi.ResponseBodies
{
    public class ReviewResponseDTO
    {
        public ReviewResponseDTO(Review review)
        {
            Id = review.Id;
            ChoreId = review.ChoreId;
            MemberId = review.MemberId;
            ReviewScore = review.ReviewScore;
        }

        public Guid Id { get; set; }
        public Guid ChoreId { get; set; }
        public Guid MemberId { get; set; }
        public EChoreReviewScore ReviewScore { get; set; }
    }
}

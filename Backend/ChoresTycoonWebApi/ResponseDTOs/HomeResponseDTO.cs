﻿using ChoresTycoonCore.Entities;
using System.Collections.ObjectModel;

namespace ChoresTycoonWebApi.ResponseBodies
{
    public class HomeResponseDTO
    {
        public Guid Id { get; private set; }
        public string Name { get; set; }
        public Collection<MemberResponseDTO> Members { get; set; } = new Collection<MemberResponseDTO>() { };
        public Collection<ChoreResponseDTO> Chores { get; set; } = new Collection<ChoreResponseDTO>() { };

        public HomeResponseDTO(Home home)
        {
            Id = home.Id;
            Name = home.Name;

            foreach(Member member in home.GetMembers())
            {
                Members.Add(new MemberResponseDTO(member));
            }

            foreach(Chore chore in home.GetChores())
            {
                Chores.Add(new ChoreResponseDTO(chore));
            }
        }
    }
}

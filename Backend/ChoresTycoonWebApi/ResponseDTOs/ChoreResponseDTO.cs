﻿using ChoresTycoonCore.Entities;
using System.Collections.ObjectModel;

namespace ChoresTycoonWebApi.ResponseBodies
{
    public class ChoreResponseDTO
    {
        public ChoreResponseDTO(Chore chore)
        {
            Id = chore.Id;
            Name = chore.Name;
            Description = chore.Description;
            Deadline = chore.Deadline;
            Completed = chore.Completed;
            HomeId = chore.HomeId;

            foreach (Member member in chore.GetAssignedMembers())
            {
                AssignedMembers.Add(new MemberResponseDTO(member));
            }

            foreach (Review review in chore.GetReviews())
            {
                Reviews.Add(new ReviewResponseDTO(review));
            }
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public bool Completed { get; set; }
        public Guid HomeId { get; set; }
        public Collection<MemberResponseDTO> AssignedMembers { get; set; } = new Collection<MemberResponseDTO>();
        public Collection<ReviewResponseDTO> Reviews { get; set; } = new Collection<ReviewResponseDTO>();
    }
}

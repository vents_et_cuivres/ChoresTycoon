﻿using ChoresTycoonCore.Entities;

namespace ChoresTycoonWebApi.ResponseBodies
{
    public class MemberResponseDTO
    {
        public MemberResponseDTO(Member member)
        {
            Id = member.Id;
            Name = member.Name;
            Surname = member.Surname;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}

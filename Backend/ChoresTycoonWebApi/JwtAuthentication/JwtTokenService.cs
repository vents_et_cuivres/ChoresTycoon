﻿using ChoresTycoonCore.Entities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ChoresTycoonWebApi.JwtAuthentication
{
    public class JwtTokenService : ITokenService
    {
        private const double EXPIRY_DURATION_MINUTES = 30;
        private string _key;
        private string _issuer;
        private string _audience;

        public JwtTokenService(IConfiguration config)
        {
            _key = config["Jwt:Key"];
            _issuer = config["Jwt:Issuer"];
            _audience = config["Jwt:Audience"];
        }

        public string BuildToken(Account account)
        {
            var claims = new[] {
                new Claim(ClaimTypes.Name, account.Id.ToString())
            };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_key));
            var signIn = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                _issuer,
                _audience,
                claims,
                expires: DateTime.UtcNow.AddMinutes(EXPIRY_DURATION_MINUTES),
                signingCredentials: signIn);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        public Guid? IsTokenValid(string token)
        {
            var mySecret = Encoding.UTF8.GetBytes(_key);
            var mySecurityKey = new SymmetricSecurityKey(mySecret);
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken;
            try
            {
                JwtSecurityToken jsonToken = tokenHandler.ReadJwtToken(token);
                var jwtSecurityToken = 
                tokenHandler.ValidateToken(token,
                new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = _issuer,
                    ValidAudience = _audience,
                    IssuerSigningKey = mySecurityKey,
                }, out validatedToken);
                return new Guid(jsonToken.Claims.First(claim => claim.Type == ClaimTypes.Name).Value);
            }
            catch
            {
                return null;
            }
        }
    }

}

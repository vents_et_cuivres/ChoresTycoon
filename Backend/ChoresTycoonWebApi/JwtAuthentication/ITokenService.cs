﻿using ChoresTycoonCore.Entities;

namespace ChoresTycoonWebApi.JwtAuthentication
{
    public interface ITokenService
    {
        public string BuildToken(Account account);
        public Guid? IsTokenValid(string token);
    }
}

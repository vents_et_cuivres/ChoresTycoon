using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonWebApi.JwtAuthentication;
using ChoresTycoonWebApi.RequestBodies;
using ChoresTycoonWebApi.ResponseBodies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.ObjectModel;

namespace ChoresTycoonWebApi.Controllers
{

    [ApiController]
    public class MemberController : ControllerBase
    {
        private ITokenService _tokenService { get; set; }
        private IHomeRepository _homeRepository { get; set; }
        private IMemberRepository _memberRepository { get; set; }
        private IAccountRepository _accountRepository { get; set; }
        private IChoreRepository _choreRepository { get; set; }
        public MemberController(IConfiguration config)
        {
            string connectionString = config["DatabaseConnectionStrings:Production"];
            _homeRepository = new MysqlHomeRepository(connectionString);
            _memberRepository = new MysqlMemberRepository(connectionString);
            _accountRepository = new MysqlAccountRepository(connectionString);
            _choreRepository = new MysqlChoreRepository(connectionString);
            _tokenService = new JwtTokenService(config);
        }

        [HttpPost]
        [Authorize]
        [Route("api/member/create")]
        public IActionResult PostCreateMember(MemberRequestDTO memberRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);


            Member createdMember = new Member(memberRequestBody.Name, memberRequestBody.Surname);
            var createMemberUsecase = new CreateMember(_memberRepository);
            createMemberUsecase.Execute(createdMember);

            return Ok(new { Status = "Ok", Message = "Member successfully created." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/member/delete")]
        public IActionResult PostDeleteMember(MemberRequestDTO memberRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);


            Member memberToDelete = _memberRepository.Get(memberRequestBody.memberId);
            var deleteMemberUsecase = new DeleteMember(_memberRepository);
            deleteMemberUsecase.Execute(memberToDelete);

            return Ok(new { Status = "Ok", Message = "Member successfully deleted." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/member/modify")]
        public IActionResult PostModifyMember(MemberRequestDTO memberRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            Member memberToModify = _memberRepository.Get(memberRequestBody.memberId);
            var modifyMemberUsecase = new ModifyMember(_memberRepository);
            memberToModify.Name = memberRequestBody.Name;
            memberToModify.Surname = memberRequestBody.Surname;
            modifyMemberUsecase.Execute(memberToModify);

            return Ok(new { Status = "Ok", Message = "Member successfully modified." });
        }

        [HttpGet]
        [Authorize]
        [Route("api/member/search")]
        public IActionResult GetSearchMember()
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var searchMemberUsecase = new SearchMembers(_memberRepository);
            Collection<Member> allMembers = searchMemberUsecase.Execute();

            Collection<MemberResponseDTO> memberResponseBodies = new Collection<MemberResponseDTO>();
            foreach(Member member in allMembers)
            {
                memberResponseBodies.Add(new MemberResponseDTO(member));
            }
            return Ok(memberResponseBodies);
        }

        [HttpPost]
        [Authorize]
        [Route("api/member/getChores")]
        public IActionResult PostChoresFromMember(MemberRequestDTO memberRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var getChoresFromMember = new GetChoresFromMember(_choreRepository);
            Collection<Chore> choresFromMember = getChoresFromMember.Execute(memberRequestBody.memberId);

            Collection<ChoreResponseDTO> choresResponseBodies = new Collection<ChoreResponseDTO>();
            foreach (Chore chore in choresFromMember)
            {
                choresResponseBodies.Add(new ChoreResponseDTO(chore));
            }
            return Ok(choresResponseBodies);
        }

        [HttpPost]
        [Authorize]
        [Route("api/member/getHomes")]
        public IActionResult PostHomesFromMember(MemberRequestDTO memberRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var getHomesFromMember = new GetHomesFromMember(_homeRepository);
            Collection<Home> homesFromMember = getHomesFromMember.Execute(memberRequestBody.memberId);

            Collection<HomeResponseDTO> homesResponseBodies = new Collection<HomeResponseDTO>();
            foreach (Home home in homesFromMember)
            {
                homesResponseBodies.Add(new HomeResponseDTO(home));
            }
            return Ok(homesResponseBodies);
        }
    }
}
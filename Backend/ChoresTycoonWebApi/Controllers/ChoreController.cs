using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.DataTypes;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonWebApi.JwtAuthentication;
using ChoresTycoonWebApi.RequestBodies;
using ChoresTycoonWebApi.ResponseBodies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChoresTycoonWebApi.Controllers
{
    [ApiController]
    public class ChoreController : ControllerBase
    {
        private ITokenService _tokenService { get; set; }
        private IHomeRepository _homeRepository { get; set; }
        private IMemberRepository _memberRepository { get; set; }
        private IAccountRepository _accountRepository { get; set; }
        private IChoreRepository _choreRepository { get; set; }
        private IReviewRepository _reviewRepository { get; set; }
        public ChoreController(IConfiguration config)
        {
            string connectionString = config["DatabaseConnectionStrings:Production"];
            _homeRepository = new MysqlHomeRepository(connectionString);
            _memberRepository = new MysqlMemberRepository(connectionString);
            _accountRepository = new MysqlAccountRepository(connectionString);
            _choreRepository = new MysqlChoreRepository(connectionString);
            _reviewRepository = new MysqlReviewRepository(connectionString);
            _tokenService = new JwtTokenService(config);
        }

        [HttpPost]
        [Authorize]
        [Route("api/chore/create")]
        public IActionResult PostCreateChore(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            string description = choreRequestBody.description;
            DateTime deadline = new ChoreDeadline(choreRequestBody.deadline);

            Chore createChore = new Chore(choreRequestBody.name, description, deadline, choreRequestBody.homeId);
            Home homeWhereChoreisCreated = _homeRepository.Get(choreRequestBody.homeId);
            var createChoreUsecase = new CreateChore(_choreRepository, _homeRepository);
            createChoreUsecase.Execute(homeWhereChoreisCreated, createChore);

            return Ok(new { Status = "Ok", Message = "Chore successfully created." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/chore/delete")]
        public IActionResult PostDeleteChore(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            Chore choreToDelete = _choreRepository.Get(choreRequestBody.choreId);
            var deleteChoreUsecase = new DeleteChore(_choreRepository);
            deleteChoreUsecase.Execute(choreToDelete);

            return Ok(new { Status = "Ok", Message = "Chore successfully deleted." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/chore/modify")]
        public IActionResult PostModifyChore(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            Chore choreToModify = _choreRepository.Get(choreRequestBody.choreId);
            var modifyChoreUsecase = new ModifyChore(_choreRepository);

            string description = choreRequestBody.description;
            DateTime deadline = new ChoreDeadline(choreRequestBody.deadline);


            choreToModify.Name= choreRequestBody.name;
            choreToModify.Description = description;
            choreToModify.Deadline = deadline;

            modifyChoreUsecase.Execute(choreToModify);

            return Ok(new { Status = "Ok", Message = "Chore successfully modified." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/chore/complete")]
        public IActionResult PostCompleteChore(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var completeChoreUsecase = new CompleteChore(_choreRepository);
            var chore = _choreRepository.Get(choreRequestBody.choreId);

            completeChoreUsecase.Execute(chore);

            return Ok(new { Status = "Ok", Message = "Chore successfully modified." });
        }


        //ADD REVIEW N'EST PAS REDONDANT ?

        [HttpPost]
        [Authorize]
        [Route("api/chore/assignMemberToChore")]
        public IActionResult PostAssignMemberToChore(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var assignMemberToChore = new AssignMemberToChore(_choreRepository, _memberRepository);

            Chore chore = _choreRepository.Get(choreRequestBody.choreId);
            Member memberToAdd = _memberRepository.Get(choreRequestBody.memberId);

            assignMemberToChore.Execute(chore, memberToAdd);

            return Ok(new { Status = "Ok", Message = "Member successfully assigned to chore." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/chore/removeMemberFromChore")]
        public IActionResult PostRemoveMemberFromChore(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var removeMemberFromChore = new RemoveMemberFromChore(_choreRepository, _memberRepository);

            Chore chore = _choreRepository.Get(choreRequestBody.choreId);
            Member memberToRemove = _memberRepository.Get(choreRequestBody.memberId);

            removeMemberFromChore.Execute(chore, memberToRemove);

            return Ok(new { Status = "Ok", Message = "Member successfully removed from chore." });
        }


        [HttpPost]
        [Authorize]
        [Route("api/chore/addReviewToChore")]
        public IActionResult PostAddReviewToChore(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var addReviewToChore = new AddReviewToChore(_choreRepository);

            Review review = _reviewRepository.Get(choreRequestBody.reviewId);
            Chore chore = _choreRepository.Get(choreRequestBody.choreId);

            addReviewToChore.Execute(chore, review);

            return Ok(new { Status = "Ok", Message = "Review successfully added to chore." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/chore/readChore")]
        public IActionResult PostReadWholeChore(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            RetrieveChoreWithMembersAndReviews retrieveWholeChoreUsecase = new RetrieveChoreWithMembersAndReviews(_choreRepository, _memberRepository, _reviewRepository);
            Chore chore = retrieveWholeChoreUsecase.Execute(choreRequestBody.choreId);

            if (chore == null)
                return BadRequest(new { Status = "Error", Message = "Chore not found." });

            return Ok(new ChoreResponseDTO(chore));
        }
    }
}
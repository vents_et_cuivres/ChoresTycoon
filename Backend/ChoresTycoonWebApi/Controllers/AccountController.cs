﻿using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Exceptions;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonWebApi.JwtAuthentication;
using ChoresTycoonWebApi.RequestBodies;
using ChoresTycoonWebApi.ResponseBodies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ChoresTycoonWebApi.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class JwtTokenResponse
    {
        public string Token { get; set; }

        public JwtTokenResponse(string token)
        {
            Token = token;
        }
    }

    [ApiController]
    public class AccountController : ControllerBase
    {

        private IAccountRepository _accountRepository { get; set; }
        private IMemberRepository _memberRepository { get; set; }
        private ITokenService _tokenService { get; set; }
        private IConfiguration _config { get; set; }

        public AccountController(IConfiguration config)
        {
            _config = config;
            string connectionString = config["DatabaseConnectionStrings:Production"];
            _accountRepository = new MysqlAccountRepository(connectionString);
            _memberRepository = new MysqlMemberRepository(connectionString);
            _tokenService = new JwtTokenService(config);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/login")]
        public IActionResult PostLogin(AccountRequestDTO acccountRequestBody)
        {
            if (string.IsNullOrWhiteSpace(acccountRequestBody.Email) || string.IsNullOrWhiteSpace(acccountRequestBody.Password))
                return BadRequest(new { Status = "Error", Message = "Password or email is empty." });

            LoginAccount loginAccountUsecase = new LoginAccount(_accountRepository);
            bool loginIsSuccessful = loginAccountUsecase.Execute(acccountRequestBody.Email, acccountRequestBody.Password);

            if (loginIsSuccessful)
            {
                Account account = _accountRepository.GetAccountByEmail(acccountRequestBody.Email);
                Response.Headers.Authorization = $"Bearer {{ {new JwtTokenResponse(_tokenService.BuildToken(account)).Token} }}";
                return Ok(new {Token = new JwtTokenResponse(_tokenService.BuildToken(account)).Token });
            }                
            else
                return BadRequest(new {Status = "Error", Message = "Wrong password or email."});
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/signup")]
        public IActionResult PostSignup(AccountRequestDTO acccountRequestBody)
        {
            if (string.IsNullOrWhiteSpace(acccountRequestBody.Email) || string.IsNullOrWhiteSpace(acccountRequestBody.Password))
                return BadRequest(new { Status = "Error", Message = "Password or email is empty." });

            var account = new Account(acccountRequestBody.Email, acccountRequestBody.Password);
            CreateAccount createAccountUsecase = new CreateAccount(_accountRepository, _memberRepository);
            createAccountUsecase.Execute(account);

            return Ok(new { Status = "Ok", Message = "User signed up." });
        }

        [HttpGet]
        [Authorize]
        [Route("api/account/delete")]
        public IActionResult GetDeleteAccount()
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var account = _accountRepository.Get((Guid)accountGuid);
            DeleteAccount deleteAccountUsecase = new DeleteAccount(_accountRepository);
            deleteAccountUsecase.Execute(account);

            return Ok(new { Status = "Ok", Message = "User successfully deleted." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/account/update")]
        public IActionResult PostUpdateAccount(AccountRequestDTO acccountRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var account = _accountRepository.Get((Guid)accountGuid);

            string newMail = string.IsNullOrWhiteSpace(acccountRequestBody.Email) ? account.Email : acccountRequestBody.Email;
            string newPassword = string.IsNullOrEmpty(acccountRequestBody.Password) ? account.Password : acccountRequestBody.Password;

            try
            {
                account.Email = newMail;
                account.Password = newPassword;
                ModifyAccount modifyAccountUsecase = new ModifyAccount(_accountRepository);
                modifyAccountUsecase.Execute(account);
            }
            catch (InvalidMemberEmailException e)
            {
                return BadRequest(new { Status = "Error", Message = e.Message });
            }
            return Ok(new { Status = "Ok", Message = "User successfully updated." });
        }

        [HttpGet]
        [Authorize]
        [Route("api/account/getMember")]
        public IActionResult GetMemberFromConnectedUser()
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var account = _accountRepository.Get((Guid)accountGuid);

            return Ok(new MemberResponseDTO(_memberRepository.Get(account.MemberId)));
        }

    }
}

﻿using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonWebApi.JwtAuthentication;
using ChoresTycoonWebApi.RequestBodies;
using ChoresTycoonWebApi.ResponseBodies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.ObjectModel;

namespace ChoresTycoonWebApi.Controllers
{
    [ApiController]
    public class HomeController : ControllerBase
    {
        private ITokenService _tokenService { get; set; }
        private IHomeRepository _homeRepository { get; set; }
        private IMemberRepository _memberRepository { get; set; }
        private IAccountRepository _accountRepository { get; set; }
        private IChoreRepository _choreRepository { get; set; }
        public HomeController(IConfiguration config)
        {
            string connectionString = config["DatabaseConnectionStrings:Production"];
            _homeRepository = new MysqlHomeRepository(connectionString);
            _memberRepository = new MysqlMemberRepository(connectionString);
            _accountRepository = new MysqlAccountRepository(connectionString);
            _choreRepository = new MysqlChoreRepository(connectionString);
            _tokenService = new JwtTokenService(config);
        }

        [HttpPost]
        [Authorize]
        [Route("api/home/create")]
        public IActionResult PostCreateHome(HomeRequestDTO homeRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            Account account = _accountRepository.Get((Guid)accountGuid);
            Member accountMember = _memberRepository.Get(account.MemberId);

            Home createdHome = new Home(homeRequestBody.Name);
            var createHomeUsecase = new CreateHome(_homeRepository);
            createHomeUsecase.Execute(createdHome);

            var addMemberToHome = new AddMemberToHome(_homeRepository);
            addMemberToHome.Execute(createdHome, accountMember);

            return Ok(new { Status = "Ok", Message = "Home successfully created." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/home/delete")]
        public IActionResult PostDeleteHome(HomeRequestDTO homeRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            
            var deleteHomeUsecase = new DeleteHome(_homeRepository);
            deleteHomeUsecase.Execute(_homeRepository.Get(homeRequestBody.homeId));  

            return Ok(new { Status = "Ok", Message = "Home successfully deleted." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/home/modify")]
        public IActionResult PostModifyHome(HomeRequestDTO homeRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

         
            var modifyHomeUsecase = new ModifyHome(_homeRepository);
            Home modifiedHome = _homeRepository.Get(homeRequestBody.homeId);
            modifiedHome.Name = homeRequestBody.Name;
            modifyHomeUsecase.Execute(modifiedHome);

            return Ok(new { Status = "Ok", Message = "Home successfully modified." });
        }


        [HttpPost]
        [Authorize]
        [Route("api/home/addMemberToHome")]
        public IActionResult PostAddMemberToHome(HomeRequestDTO homeRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);


            Home homeToModify = _homeRepository.Get(homeRequestBody.homeId);
            Member memberToAdd = _memberRepository.Get(homeRequestBody.memberId);
            var addMemberToHome = new AddMemberToHome(_homeRepository);
            addMemberToHome.Execute(homeToModify, memberToAdd);


            return Ok(new { Status = "Ok", Message = "Member successfully added." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/home/removeMemberFromHome")]
        public IActionResult PostRemoveMemberFromHome(HomeRequestDTO homeRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);


            Home homeToModify = _homeRepository.Get(homeRequestBody.homeId);
            Member memberToRemove = _memberRepository.Get(homeRequestBody.memberId);
            var removeMemberFromHome = new RemoveMemberFromHome(_homeRepository);
            removeMemberFromHome.Execute(homeToModify, memberToRemove);


            return Ok(new { Status = "Ok", Message = "Member successfully removed." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/home/readHome")]
        public IActionResult PostReadWholeHome(HomeRequestDTO homeRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            var retrieveWholeHomeUsecase = new RetrieveHomeWithChoresAndMembers(_homeRepository, _memberRepository, _choreRepository);
            Home home = retrieveWholeHomeUsecase.Execute(homeRequestBody.homeId);

            if (home == null)
                return BadRequest(new { Status = "Error", Message = "Home not found." });

            return Ok(new HomeResponseDTO(home));
        }

        [HttpPost]
        [Authorize]
        [Route("api/home/getAllChoresFromHome")]
        public IActionResult PostAllChoresFromHome(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            Home home = _homeRepository.Get(choreRequestBody.homeId);

            if (home == null)
                return BadRequest(new { Status = "Error", Message = "Home not found." });

            GetChoresFromHome getChoresFromHomeUsecase = new GetChoresFromHome(_choreRepository);
            Collection<Chore> retrievedChores = getChoresFromHomeUsecase.Execute(home);

            Collection<ChoreResponseDTO> choresToSend = new Collection<ChoreResponseDTO>();
            foreach (Chore chore in retrievedChores)
                choresToSend.Add(new ChoreResponseDTO(chore));

            return Ok(choresToSend);
        }

        [HttpPost]
        [Authorize]
        [Route("api/home/getAllMembersFromHome")]
        public IActionResult PostAllMembersFromHome(ChoreRequestDTO choreRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            Home home = _homeRepository.Get(choreRequestBody.homeId);

            if (home == null)
                return BadRequest(new { Status = "Error", Message = "Home not found." });

            GetMembersFromHome getMembersFromHomeUsecase = new GetMembersFromHome(_memberRepository);
            Collection<Member> retrievedMembers = getMembersFromHomeUsecase.Execute(home);

            Collection<MemberResponseDTO> membersToSend = new Collection<MemberResponseDTO>();
            foreach (Member member in retrievedMembers)
                membersToSend.Add(new MemberResponseDTO(member));

            return Ok(membersToSend);
        }
    }
}

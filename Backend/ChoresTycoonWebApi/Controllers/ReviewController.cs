using ChoresTycoonCore.Adapters;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using ChoresTycoonCore.UseCases;
using ChoresTycoonWebApi.JwtAuthentication;
using ChoresTycoonWebApi.RequestBodies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ChoresTycoonCore.Enums;

namespace ChoresTycoonWebApi.Controllers
{
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private ITokenService _tokenService { get; set; }
        private IHomeRepository _homeRepository { get; set; }
        private IMemberRepository _memberRepository { get; set; }
        private IAccountRepository _accountRepository { get; set; }
        private IChoreRepository _choreRepository { get; set; }
        private IReviewRepository _reviewRepository { get; set; }

        public ReviewController(IConfiguration config)
        {
            string connectionString = config["DatabaseConnectionStrings:Production"];
            _homeRepository = new MysqlHomeRepository(connectionString);
            _memberRepository = new MysqlMemberRepository(connectionString);
            _accountRepository = new MysqlAccountRepository(connectionString);
            _choreRepository = new MysqlChoreRepository(connectionString);
            _reviewRepository = new MysqlReviewRepository(connectionString);
            _tokenService = new JwtTokenService(config);
        }

        [HttpPost]
        [Authorize]
        [Route("api/review/create")]
        public IActionResult PostCreateReview(ReviewRequestDTO reviewRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);

            Member memberReviewing= _memberRepository.Get(reviewRequestBody.MemberId);
            Chore choreReviewed = _choreRepository.Get(reviewRequestBody.ChoreId);
            EChoreReviewScore score = reviewRequestBody.ReviewScore;

            Review createdReview = new Review(memberReviewing, choreReviewed, score);
            var createReviewUsecase = new CreateReview(_reviewRepository, _choreRepository,  _homeRepository);
            createReviewUsecase.Execute(choreReviewed, createdReview);

            return Ok(new { Status = "Ok", Message = "Review successfully created." });
        }


        [HttpPost]
        [Authorize]
        [Route("api/review/delete")]
        public IActionResult PostDeleteReview(ReviewRequestDTO reviewRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);


            Review reviewToDelete = _reviewRepository.Get(reviewRequestBody.ReviewId);
            var deleteReviewUsecase = new DeleteReview(_reviewRepository);
            deleteReviewUsecase.Execute(reviewToDelete.Id);

            return Ok(new { Status = "Ok", Message = "Review successfully deleted." });
        }

        [HttpPost]
        [Authorize]
        [Route("api/review/modify")]
        public IActionResult PostModifyReview(ReviewRequestDTO reviewRequestBody)
        {
            var token = HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1];
            Guid? accountGuid = _tokenService.IsTokenValid(token);
            if (accountGuid == null)
                return StatusCode(StatusCodes.Status500InternalServerError);


            Review reviewToModify = _reviewRepository.Get(reviewRequestBody.ReviewId);

            reviewToModify.ReviewScore = reviewRequestBody.ReviewScore;


            var modifyReviewUsecase = new ModifyReview(_reviewRepository);
            modifyReviewUsecase.Execute(reviewToModify);

            return Ok(new { Status = "Ok", Message = "Review successfully updated." });
        }





    }
}

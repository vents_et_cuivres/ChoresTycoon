﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Enums
{
    public enum EChoreReviewScore
    {
        WellDone = 3,
        Done = 2,
        BadlyDone = 1,
        NotDone = 0
    }
}

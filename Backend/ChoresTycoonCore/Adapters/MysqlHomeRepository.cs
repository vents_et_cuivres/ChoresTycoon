﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Adapters
{
    public class MysqlHomeRepository : IHomeRepository
    {
        private MySqlConnection connection { get; set; }

        public MysqlHomeRepository(string connectionString)
        {
            this.connection = new MySqlConnection(connectionString);
        }

        public MysqlHomeRepository()
        {
            string serverIp = "127.0.0.1";
            string port = "3306";
            string username = "root";
            string password = "blopblop";

            string connectionString = @$"server={serverIp};
                                         port={port};
                                         user id={username};
                                         password={password};
                                         database=ChoresTycoon;
                                         SslMode=none;
                                         AllowPublicKeyRetrieval=true";

            connection = new MySqlConnection(connectionString);
        }

        public void Create(Home Home)
        {
            string sqlRequest = "INSERT INTO Home (id, name) VALUES (@homeId, @homeName)";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", Home.Id);
            sqlCommand.Parameters.AddWithValue("@homeName", (string)Home.Name);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public IReadOnlyCollection<Home> GetAll()
        {
            var homes = new List<Home>();

            string sqlRequest = "SELECT * FROM Home";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    var guid = dataReader.GetGuid("id");
                    var name = dataReader.GetString("name");

                    homes.Add(new Home(guid, name));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return homes;
        }

        public Home? Get(Guid homeId)
        {
            Home home = null;
            string sqlRequest = "SELECT * FROM Home WHERE id = @homeId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", homeId);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    var guid = dataReader.GetGuid("id");
                    var name = dataReader.GetString("name");

                    home = new Home(guid, name);
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return home;
        }

        public void Update(Home home)
        {
            string sqlRequest = @"UPDATE Home SET name = @homeName WHERE id = @homeId";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeName", (string)home.Name);
            sqlCommand.Parameters.AddWithValue("@homeId", home.Id);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public void Delete(Guid homeId)
        {
            string sqlRequest = "DELETE FROM Home WHERE id = @homeId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", homeId);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        //ADDED FOR TEST
        public void DeleteHomeMembers(Guid homeId)
        {
            string sqlRequest = "DELETE FROM HomeMembers WHERE home_id = @homeId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", homeId);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public void AddMemberToHome(Home home, Member member)
        {
            string sqlRequest = "INSERT INTO HomeMembers (home_id, member_id) VALUES (@homeId, @memberId)";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", home.Id);
            sqlCommand.Parameters.AddWithValue("@memberId", member.Id);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public void RemoveMemberFromHome(Home home, Member member)
        {
            string sqlRequest = "DELETE FROM HomeMembers WHERE home_id = @homeId AND member_id = @memberId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", home.Id);
            sqlCommand.Parameters.AddWithValue("@memberId", member.Id);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public bool MemberBelongsToHome(Guid homeId, Guid memberId)
        {
            bool memberBelongsToHome = false;
            string sqlRequest = "SELECT * FROM HomeMembers WHERE home_id = @homeId and member_id = @memberId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", homeId);
            sqlCommand.Parameters.AddWithValue("@memberId", memberId);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();
                memberBelongsToHome = dataReader.HasRows;
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return memberBelongsToHome;
        }

        public Collection<Home> GetAllHomesFromMember(Guid memberId)
        {
            Collection<Home> homes = new Collection<Home>();
            foreach (Guid homeId in GetAllHomesGuidFromMember(memberId))
            {
                Home retrievedHome = Get(homeId);
                if (retrievedHome != null)
                    homes.Add(retrievedHome);
            }
            return homes;
        }

        private Collection<Guid> GetAllHomesGuidFromMember(Guid memberId)
        {
            var homesGuid = new Collection<Guid>();
            string sqlRequest = "SELECT * FROM HomeMembers WHERE member_id = @memberId";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@memberId", memberId);
            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    homesGuid.Add(dataReader.GetGuid("home_id"));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return homesGuid;
        }
    }
}

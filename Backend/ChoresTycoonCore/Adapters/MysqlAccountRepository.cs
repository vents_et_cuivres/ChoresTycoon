﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Adapters
{
    public class MysqlAccountRepository : IAccountRepository
    {
        private MySqlConnection connection { get; set; }

        public MysqlAccountRepository(string connectionString)
        {
            this.connection = new MySqlConnection(connectionString);
        }

        public MysqlAccountRepository()
        {
            string serverIp = "127.0.0.1";
            string port = "3306";
            string username = "root";
            string password = "blopblop";

            string connectionString = @$"server={serverIp};
                                         port={port};
                                         user id={username};
                                         password={password};
                                         database=ChoresTycoon;
                                         SslMode=none;
                                         AllowPublicKeyRetrieval=true";

            connection = new MySqlConnection(connectionString);
        }

        public void Create(Account account)
        {
            string sqlRequest = "INSERT INTO Account (id, email, password, member_id) VALUES (@accountId, @accountEmail, @accountPassword, @memberId)";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@accountId", account.Id);
            sqlCommand.Parameters.AddWithValue("@accountEmail", (string)account.Email);
            sqlCommand.Parameters.AddWithValue("@accountPassword", (string)account.Password);
            sqlCommand.Parameters.AddWithValue("@memberId", account.MemberId);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public void Delete(Guid accountId)
        {
            string sqlRequest = "DELETE FROM Account WHERE id = @accountId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@accountId", accountId);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public Account? Get(Guid accountId)
        {
            Account account = null;
            string sqlRequest = "SELECT * FROM Account WHERE id = @accountId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@accountId", accountId);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    var guid = dataReader.GetGuid("id");
                    var email = dataReader.GetString("email");
                    var password = dataReader.GetString("password");
                    var memberId = dataReader.GetGuid("member_id");

                    account = new Account(guid, email, password, memberId);
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return account;
        }

        public Account? GetAccountByEmail(string accountEmail)
        {
            Account account = null;
            string sqlRequest = "SELECT * FROM Account WHERE email = @accountEmail";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@accountEmail", (string)accountEmail);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var guid = dataReader.GetGuid("id");
                        var email = dataReader.GetString("email");
                        var password = dataReader.GetString("password");
                        var memberId = dataReader.GetGuid("member_id");

                        account = new Account(guid, email, password, memberId);
                    }
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return account;
        }

        public IReadOnlyCollection<Account> GetAll()
        {
            var accounts = new List<Account>();

            string sqlRequest = "SELECT * FROM Account";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    var guid = dataReader.GetGuid("id");
                    var email = dataReader.GetString("email");
                    var password = dataReader.GetString("password");
                    var memberId = dataReader.GetGuid("memberId");

                     accounts.Add(new Account(guid, email, password, memberId));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return accounts;
        }

        public void Update(Account account)
        {
            string sqlRequest = "UPDATE Account SET email = @accountEmail, password = @accountPassword WHERE id = @accountId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@accountId", account.Id);
            sqlCommand.Parameters.AddWithValue("@accountEmail", (string)account.Email) ;
            sqlCommand.Parameters.AddWithValue("@accountPassword", (string)account.Password);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Enums;
using ChoresTycoonCore.Ports;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Adapters
{
    public class MysqlChoreRepository : IChoreRepository
    {
        private MySqlConnection connection { get; set; }

        public MysqlChoreRepository(string connectionString)
        {
            this.connection = new MySqlConnection(connectionString);
        }

        public MysqlChoreRepository()
        {
            string serverIp = "127.0.0.1";
            string port = "3306";
            string username = "root";
            string password = "blopblop";

            string connectionString = @$"server={serverIp};
                                         port={port};
                                         user id={username};
                                         password={password};
                                         database=ChoresTycoon;
                                         SslMode=none;
                                         AllowPublicKeyRetrieval=true";

            connection = new MySqlConnection(connectionString);
        }

        public void Create(Chore chore)
        {
            string sqlRequest = @"INSERT INTO Chore (id, name, description, deadline, completed, home_id) 
                                  VALUES (@choreId, @choreName, @choreDescription, @choreDeadline, @choreCompleted, @choreHomeId)";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@choreId", chore.Id);
            sqlCommand.Parameters.AddWithValue("@choreName", (string)chore.Name);
            sqlCommand.Parameters.AddWithValue("@choreDescription", (string)chore.Description);
            sqlCommand.Parameters.AddWithValue("@choreDeadline", (DateTime)chore.Deadline);
            sqlCommand.Parameters.AddWithValue("@choreCompleted", chore.Completed);
            sqlCommand.Parameters.AddWithValue("@choreHomeId", chore.HomeId);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public IReadOnlyCollection<Chore> GetAll()
        {
            var chores = new List<Chore>();

            string sqlRequest = "SELECT * FROM Chore";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    var guid = dataReader.GetGuid("id");
                    var name = dataReader.GetString("name");
                    var description = dataReader.GetString("description");
                    var deadline = dataReader.GetDateTime("deadline");
                    var completed = dataReader.GetBoolean("completed");
                    var homeId = dataReader.GetGuid("home_id");

                    chores.Add(new Chore(guid, completed, name, description, deadline, homeId));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return chores;
        }

        public Chore? Get(Guid choreId)
        {
            Chore chore = null;
            string sqlRequest = "SELECT * FROM Chore WHERE id = @choreId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@choreId", choreId);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    var guid = dataReader.GetGuid("id");
                    var name = dataReader.GetString("name");
                    var description = dataReader.GetString("description");
                    var deadline = dataReader.GetDateTime("deadline");
                    var completed = dataReader.GetBoolean("completed");
                    var homeId = dataReader.GetGuid("home_id");

                    chore = new Chore(guid, completed, name, description, deadline, homeId);
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return chore;
        }
        public void Update(Chore chore)
        {
            string sqlRequest = @"UPDATE Chore 
                                  SET name = @choreName, description = @choreDescription, completed = @choreCompleted, deadline = @choreDeadline
                                  WHERE id = @choreId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@choreId", chore.Id);
            sqlCommand.Parameters.AddWithValue("@choreName", (string)chore.Name);
            sqlCommand.Parameters.AddWithValue("@choreDescription", (string)chore.Description);
            sqlCommand.Parameters.AddWithValue("@choreCompleted", chore.Completed);
            sqlCommand.Parameters.AddWithValue("@choreDeadline", (DateTime)chore.Deadline);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public void Delete(Guid choreID)
        {
            string sqlRequest = "DELETE FROM Chore WHERE id = @choreId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@choreId", choreID);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public Collection<Chore> GetAllChoresFromHome(Home home)
        {
            var chores = new Collection<Chore>();

            string sqlRequest = "SELECT * FROM Chore WHERE home_id = @homeId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", home.Id);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    var guid = dataReader.GetGuid("id");
                    var name = dataReader.GetString("name");
                    var description = dataReader.GetString("description");
                    var deadline = dataReader.GetDateTime("deadline");
                    var completed = dataReader.GetBoolean("completed");
                    var homeId = dataReader.GetGuid("home_id");

                    chores.Add(new Chore(guid, completed, name, description, deadline, homeId));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return chores;
        }

        public Collection<Chore> GetAllChoresFromMember(Guid memberId)
        {
            Collection<Chore> chores = new Collection<Chore>();
            foreach (Guid choreGuid in GetAllChoresGuidFromMember(memberId))
            {
                Chore retrivedChore = Get(choreGuid);
                if (retrivedChore != null)
                    chores.Add(retrivedChore);
            }
            return chores;
        }

        private Collection<Guid> GetAllChoresGuidFromMember(Guid memberId)
        {
            var choresGuid = new Collection<Guid>();
            string sqlRequest = "SELECT * FROM ChoreMembers WHERE member_id = @memberId";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@memberId", memberId);
            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    choresGuid.Add(dataReader.GetGuid("chore_id"));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return choresGuid;
        }
    }
}

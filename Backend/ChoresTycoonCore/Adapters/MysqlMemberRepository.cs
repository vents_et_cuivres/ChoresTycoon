﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Adapters
{
    public class MysqlMemberRepository : IMemberRepository
    {
        private MySqlConnection connection { get; set; }

        public MysqlMemberRepository(string connectionString)
        {
            connection = new MySqlConnection(connectionString);
        }

        public MysqlMemberRepository()
        {
            string serverIp = "127.0.0.1";
            string port = "3306";
            string username = "root";
            string password = "blopblop";

            string connectionString = @$"server={serverIp};
                                         port={port};
                                         user id={username};
                                         password={password};
                                         database=ChoresTycoon;
                                         SslMode=none;
                                         AllowPublicKeyRetrieval=true";

            connection = new MySqlConnection(connectionString);
        }

        public void Create(Member member)
        {
            string sqlRequest = "INSERT INTO Member (id, surname, name) VALUES (@memberId, @memberSurname, @memberName)";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@memberId", member.Id);
            sqlCommand.Parameters.AddWithValue("@memberSurname", (string)member.Surname);
            sqlCommand.Parameters.AddWithValue("@memberName", (string)member.Name);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }

        }

        public void Delete(Guid memberId)
        {
            string sqlRequest = "DELETE FROM Member WHERE id = @memberId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@memberId", memberId);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public Member? Get(Guid memberId)
        {
            Member member = null;
            string sqlRequest = "SELECT * FROM Member WHERE id = @memberId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@memberId", memberId);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                
                while (dataReader.Read())
                {                  
                    var guid = dataReader.GetGuid("id");
                    var surname = dataReader.GetString("surname");
                    var name = dataReader.GetString("name");

                    member = new Member(guid, surname, name);
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return member;
        }

        public IReadOnlyCollection<Member> GetAll()
        {
            var members = new List<Member>();

            string sqlRequest = "SELECT * FROM Member";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    var guid = dataReader.GetGuid("id");
                    var surname = dataReader.GetString("surname");
                    var name = dataReader.GetString("name");

                    members.Add(new Member(guid, surname, name));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return members;
        }

        public void Update(Member member)
        {
            string sqlRequest = "UPDATE Member SET surname = @memberSurname, name = @memberName WHERE id = @memberId";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@memberId", member.Id);
            sqlCommand.Parameters.AddWithValue("@memberSurname", (string)member.Surname);
            sqlCommand.Parameters.AddWithValue("@memberName", (string)member.Name);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public Collection<Member> GetAllMembersFromHome(Home home)
        {
            var members = new Collection<Member>();

            foreach(Guid memberGuid in GetAllMembersGuidFromHome(home.Id))
            {
                Member retrievedMember = Get(memberGuid);
                if (retrievedMember != null)
                    members.Add(retrievedMember);
            }

            return members;
        }

        private Collection<Guid> GetAllMembersGuidFromHome(Guid homeId)
        {
            var membersGuid = new Collection<Guid>();
            string sqlRequest = "SELECT * FROM HomeMembers WHERE home_id = @homeId";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@homeId", homeId);
            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    membersGuid.Add(dataReader.GetGuid("member_id"));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return membersGuid;
        }

        public Collection<Member> GetAllMembersAssignedToChore(Chore chore)
        {
            Collection<Member> members = new Collection<Member>();
            foreach(Guid memberGuid in GetAllMembersGuidAssignedToChore(chore.Id))
            {
                Member retrievedMember = Get(memberGuid);
                if (retrievedMember != null)
                    members.Add(retrievedMember);
            }
            return members;
        }

        private Collection<Guid> GetAllMembersGuidAssignedToChore(Guid choreId)
        {
            var membersGuid = new Collection<Guid>();
            string sqlRequest = "SELECT * FROM ChoreMembers WHERE chore_id = @choreId";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@choreId", choreId);
            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    membersGuid.Add(dataReader.GetGuid("member_id"));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return membersGuid;
        }

        public void AssignMemberToChore(Guid choreId, Guid memberId)
        {
            string sqlRequest = "INSERT INTO ChoreMembers (chore_id, member_id) VALUES (@choreId, @memberId)";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@choreId", choreId);
            sqlCommand.Parameters.AddWithValue("@memberId", memberId);
            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public void RemoveMemberFromChore(Guid choreId, Guid memberId)
        {
            string sqlRequest = "DELETE FROM ChoreMembers WHERE chore_id = @choreId AND member_id = @memberId";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@choreId", choreId);
            sqlCommand.Parameters.AddWithValue("@memberId", memberId);
            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Enums;
using ChoresTycoonCore.Ports;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Adapters
{
    public class MysqlReviewRepository : IReviewRepository
    {
        private MySqlConnection connection { get; set; }

        public MysqlReviewRepository(string connectionString)
        {
            this.connection = new MySqlConnection(connectionString);
        }

        public MysqlReviewRepository()
        {
            string serverIp = "127.0.0.1";
            string port = "3306";
            string username = "root";
            string password = "blopblop";

            string connectionString = @$"server={serverIp};
                                         port={port};
                                         user id={username};
                                         password={password};
                                         database=ChoresTycoon;
                                         SslMode=none;
                                         AllowPublicKeyRetrieval=true";

            connection = new MySqlConnection(connectionString);
        }
        public void Create(Review review)
        {
            string sqlRequest = "INSERT INTO Review (id, chore_id, member_id, review_score) VALUES (@reviewId, @choreId, @memberId, @reviewScore)";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@reviewId", review.Id);
            sqlCommand.Parameters.AddWithValue("@choreId", review.ChoreId);
            sqlCommand.Parameters.AddWithValue("@memberId", review.MemberId);
            sqlCommand.Parameters.AddWithValue("@reviewScore", (int)review.ReviewScore);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public void Delete(Guid reviewId)
        {
            string sqlRequest = "DELETE FROM Review WHERE id = @reviewId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@reviewId", reviewId);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }

        public Review? Get(Guid reviewId)
        {
            Review review = null;
            string sqlRequest = "SELECT * FROM Review WHERE id = @reviewId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@reviewId", reviewId);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    Guid guid = dataReader.GetGuid("id");
                    Guid choreGuid = dataReader.GetGuid("chore_id");
                    Guid memberGuid = dataReader.GetGuid("member_id");
                    int reviewScore = dataReader.GetInt16("review_score");

                    review = new Review(guid, choreGuid, memberGuid, (EChoreReviewScore)reviewScore);
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return review;
        }

        public IReadOnlyCollection<Review> GetAll()
        {
            var reviews = new Collection<Review>();

            string sqlRequest = "SELECT * FROM Review";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    Guid guid = dataReader.GetGuid("id");
                    Guid choreGuid = dataReader.GetGuid("chore_id");
                    Guid memberGuid = dataReader.GetGuid("member_id");
                    int reviewScore = dataReader.GetInt16("review_score");

                    reviews.Add(new Review(guid, choreGuid, memberGuid, (EChoreReviewScore)reviewScore));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
            return reviews;
        }

        public Collection<Review> GetAllReviewsFromChore(Chore chore)
        {
            var reviews = new Collection<Review>();

            string sqlRequest = "SELECT * FROM Review WHERE chore_id = @choreId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@choreId", chore.Id);

            try
            {
                connection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    Guid guid = dataReader.GetGuid("id");
                    Guid choreGuid = dataReader.GetGuid("chore_id");
                    Guid memberGuid = dataReader.GetGuid("member_id");
                    EChoreReviewScore reviewScore = (EChoreReviewScore)dataReader.GetInt32("review_score");
                    reviews.Add(new Review(guid, choreGuid, memberGuid, (EChoreReviewScore)reviewScore));
                }

                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;                
            }
            return reviews;
        }

        public void Update(Review review)
        {
            string sqlRequest = "UPDATE Review SET review_score = @reviewScore WHERE id = @reviewId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, connection);
            sqlCommand.Parameters.AddWithValue("@reviewId", review.Id);
            sqlCommand.Parameters.AddWithValue("@reviewScore", (int)review.ReviewScore);

            try
            {
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
            catch (MySqlException e)
            {
                connection.Close();
                throw e;
            }
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class GetChoresFromMember
    {
        private IChoreRepository _choreRepository { get; set; }

        public GetChoresFromMember(IChoreRepository choreRepository)
        {
            _choreRepository = choreRepository;
        }

        public Collection<Chore> Execute(Guid memberId)
        {
            return _choreRepository.GetAllChoresFromMember(memberId);
        }
    }
}

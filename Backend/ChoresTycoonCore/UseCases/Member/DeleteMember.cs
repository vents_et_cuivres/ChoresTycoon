﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class DeleteMember
    {
        private IMemberRepository Repository { get; set; }

        public DeleteMember(IMemberRepository repository)
        {
            Repository = repository;
        }

        public void Execute(Member member)
        {
            Repository.Delete(member.Id);
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class CreateMember
    {
        private IMemberRepository Repository { get; set; }

        public CreateMember(IMemberRepository repository)
        {
            Repository = repository;
        }

        public void Execute(Member member)
        {
            Repository.Create(member);
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class SearchMembers
    {
        private IMemberRepository _memberRepository { get; set; }

        public SearchMembers(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }

        public Collection<Member> Execute()
        {
            var retrievedMembers = new Collection<Member>();
            foreach (Member member in _memberRepository.GetAll())
            {
                retrievedMembers.Add(member);
            }
            return retrievedMembers;
        }
    }
}

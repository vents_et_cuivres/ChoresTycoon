﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class GetHomesFromMember
    {
        private IHomeRepository _homeRepository { get; set; }

        public GetHomesFromMember(IHomeRepository homeRepository)
        {
            _homeRepository = homeRepository;
        }

        public Collection<Home> Execute(Guid memberId)
        {
            return _homeRepository.GetAllHomesFromMember(memberId);
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class RetrieveChoreWithMembersAndReviews
    {
        private IChoreRepository choreRepository { get; set; }
        private IMemberRepository memberRepository { get; set; }
        private IReviewRepository reviewRepository { get; set; }

        public RetrieveChoreWithMembersAndReviews(IChoreRepository choreRepository, IMemberRepository memberRepository, IReviewRepository reviewRepository)
        {
            this.choreRepository = choreRepository;
            this.memberRepository = memberRepository;
            this.reviewRepository = reviewRepository;
        }

        public Chore? Execute(Guid choreId)
        {
            Chore retrievedChore = choreRepository.Get(choreId);
            if (retrievedChore != null)
            {
                retrievedChore.SetReviews(reviewRepository.GetAllReviewsFromChore(retrievedChore));
                retrievedChore.SetMembers(memberRepository.GetAllMembersAssignedToChore(retrievedChore));
            }
            return retrievedChore;
        }
    }
}

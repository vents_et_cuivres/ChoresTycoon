﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class RemoveMemberFromChore
    {
        private readonly IChoreRepository choreRepository;
        private readonly IMemberRepository memberRepository;

        public RemoveMemberFromChore(IChoreRepository choreRepository, IMemberRepository memberRepository)
        {
            this.choreRepository = choreRepository;
            this.memberRepository = memberRepository;
        }

        public void Execute(Chore chore, Member member)
        {
            memberRepository.RemoveMemberFromChore(chore.Id, member.Id);
            chore.RemoveMember(member);
            choreRepository.Update(chore);
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;

namespace ChoresTycoonCore.UseCases
{
    public class ModifyChore
    {
        private readonly IChoreRepository choreRepository;

        public ModifyChore(IChoreRepository choreRepository)
        {
            this.choreRepository = choreRepository;
        }

        public void Execute(Chore chore)
        {
            choreRepository.Update(chore);
        }
    }
}
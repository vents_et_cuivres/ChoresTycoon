﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class AssignMemberToChore
    {
        private readonly IChoreRepository choreRepository;
        private readonly IMemberRepository memberRepository;

        public AssignMemberToChore(IChoreRepository choreRepository, IMemberRepository memberRepository)
        {
            this.choreRepository = choreRepository;
            this.memberRepository = memberRepository;
        }

        public void Execute(Chore chore, Member member)
        {
            Collection<Chore> choresFromMember = choreRepository.GetAllChoresFromMember(member.Id);
            Chore choreWithSameId = choresFromMember.FirstOrDefault(c => c.Id == chore.Id, null);
            if (choreWithSameId == null)
            {
                memberRepository.AssignMemberToChore(chore.Id, member.Id);
                chore.AddMember(member);
                choreRepository.Update(chore);
            }
        }
    }
}

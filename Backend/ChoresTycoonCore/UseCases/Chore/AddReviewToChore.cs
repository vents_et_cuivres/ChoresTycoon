﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class AddReviewToChore
    {
        private readonly IChoreRepository choreRepository;
        public AddReviewToChore(IChoreRepository choreRepository)
        {
            this.choreRepository = choreRepository;
        }

        public void Execute(Chore chore, Review review)
        {
            chore.AddReview(review);
            choreRepository.Update(chore);
        }
    }


}

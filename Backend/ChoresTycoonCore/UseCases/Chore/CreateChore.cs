﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;

namespace ChoresTycoonCore.UseCases
{
    public class CreateChore
    {
        private readonly IChoreRepository choreRepository;
        private readonly IHomeRepository homeRepository;

        public CreateChore(IChoreRepository choreRepository, IHomeRepository homeRepository)
        {
            this.choreRepository = choreRepository;
            this.homeRepository = homeRepository;
        }

        public void Execute(Home home, Chore chore)
        {
            choreRepository.Create(chore);
            home.AddChore(chore);
            homeRepository.Update(home);
        }
    }
}
﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;

namespace ChoresTycoonCore.UseCases
{
    public class DeleteChore
    {
        private readonly IChoreRepository choreRepository;

        public DeleteChore(IChoreRepository choreRepository)
        {
            this.choreRepository = choreRepository;
        }

        public void Execute(Chore chore)
        {
            choreRepository.Delete(chore.Id);
        }
    }
}

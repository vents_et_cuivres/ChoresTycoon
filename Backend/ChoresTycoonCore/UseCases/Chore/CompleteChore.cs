﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;

namespace ChoresTycoonCore.UseCases
{
    public class CompleteChore
    {
        private IChoreRepository choreRepository;

        public CompleteChore(IChoreRepository choreRepository)
        {
            this.choreRepository = choreRepository;
        }

        public void Execute(Chore chore)
        {
            chore.Complete();
            choreRepository.Update(chore);
        }
    }
}
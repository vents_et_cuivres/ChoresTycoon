﻿using ChoresTycoonCore.DataTypes;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class LoginAccount
    {
        private IAccountRepository repository { get; set; }

        public LoginAccount(IAccountRepository repository)
        {
            this.repository = repository;
        }

        public bool Execute(AccountEmail email, string password)
        {
            var accountWithSameEmail = repository.GetAccountByEmail(email);
            return accountWithSameEmail != null && accountWithSameEmail.Password == password;
        }
    }
}

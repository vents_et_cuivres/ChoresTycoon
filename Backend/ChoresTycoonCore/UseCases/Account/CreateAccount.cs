﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class CreateAccount
    {

        private IAccountRepository accountRepository { get; set; }
        private IMemberRepository memberRepository { get; set; }

        public CreateAccount(IAccountRepository accountRepository, IMemberRepository memberRepository)
        {
            this.accountRepository = accountRepository;
            this.memberRepository = memberRepository;
        }

        public void Execute(Account account)
        {
            var accountWithSameEmail = accountRepository.GetAccountByEmail(account.Email);
            if (accountWithSameEmail != null)
                throw new Exception("An account with the same email address already exists.");

            var associatedMember = new Member("John", "Doe");            
            memberRepository.Create(associatedMember);

            account.MemberId = associatedMember.Id;
            accountRepository.Create(account);           
        }
    }
}

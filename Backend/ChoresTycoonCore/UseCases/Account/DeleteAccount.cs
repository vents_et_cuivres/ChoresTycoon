﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class DeleteAccount
    {
        private IAccountRepository repository { get; set; }

        public DeleteAccount(IAccountRepository repository)
        {
            this.repository = repository;
        }

        public void Execute(Account account)
        {
            repository.Delete(account.Id);
        }
    }
}

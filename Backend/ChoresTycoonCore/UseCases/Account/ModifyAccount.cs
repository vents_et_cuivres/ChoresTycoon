﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class ModifyAccount
    {
        private IAccountRepository repository { get; set; }

        public ModifyAccount(IAccountRepository repository)
        {
            this.repository = repository;
        }

        public void Execute(Account account)
        {
            var accountWithSameEmail = repository.GetAccountByEmail(account.Email);
            if (accountWithSameEmail != null && accountWithSameEmail.Id != account.Id)
                throw new Exception("This email address is already used.");
            repository.Update(account);
        }
    }
}

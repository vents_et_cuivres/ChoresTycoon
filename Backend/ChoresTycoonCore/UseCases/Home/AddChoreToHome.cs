﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class AddChoreToHome
    {
        private readonly IHomeRepository homeRepository;

        public AddChoreToHome(IHomeRepository homeRepository)
        {
            this.homeRepository = homeRepository;
        }

        public void Execute(Home home, Chore chore)
        {
            home.AddChore(chore);
            homeRepository.Update(home);
        }
    }
}

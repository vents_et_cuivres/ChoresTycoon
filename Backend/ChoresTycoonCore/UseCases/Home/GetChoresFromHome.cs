﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class GetChoresFromHome
    {
        private IChoreRepository _choreRepository { get; set; }

        public GetChoresFromHome(IChoreRepository choreRepository)
        {
            _choreRepository = choreRepository;
        }

        public Collection<Chore> Execute(Home home)
        {
            return _choreRepository.GetAllChoresFromHome(home);
        }
    }
}

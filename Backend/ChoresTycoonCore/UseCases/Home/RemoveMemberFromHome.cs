﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class RemoveMemberFromHome
    {
        private readonly IHomeRepository homeRepository;

        public RemoveMemberFromHome(IHomeRepository homeRepository)
        {
            this.homeRepository = homeRepository;
        }

        public void Execute(Home home, Member member)
        {
            homeRepository.RemoveMemberFromHome(home, member);
        }
    }


}

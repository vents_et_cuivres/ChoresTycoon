﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class CreateHome
    {
        private readonly IHomeRepository homeRepository;

        public CreateHome(IHomeRepository homeRepository)
        {
            this.homeRepository = homeRepository;
        }

        public void Execute(Home home)
        {
            homeRepository.Create(home);
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class AddMemberToHome
    {
        private readonly IHomeRepository homeRepository;

        public AddMemberToHome(IHomeRepository homeRepository)
        {
            this.homeRepository = homeRepository;
        }

        public void Execute(Home home, Member member)
        {
            homeRepository.AddMemberToHome(home, member);
        }
    }
}

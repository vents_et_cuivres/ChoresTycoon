﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class RetrieveHomeWithChoresAndMembers
    {
        private IHomeRepository homeRepository { get; set; }
        private IMemberRepository memberRepository { get; set; }
        private IChoreRepository choreRepository { get; set; }

        public RetrieveHomeWithChoresAndMembers(IHomeRepository homeRepository, IMemberRepository memberRepository, IChoreRepository choreRepository)
        {
            this.homeRepository = homeRepository;
            this.memberRepository = memberRepository;
            this.choreRepository = choreRepository;
        }

        public Home? Execute(Guid homeId)
        {
            Home home = homeRepository.Get(homeId);
            if (home != null)
            {
                home.SetChores(choreRepository.GetAllChoresFromHome(home));
                home.SetMembers(memberRepository.GetAllMembersFromHome(home));
            }

            return home;
        }
    }
}

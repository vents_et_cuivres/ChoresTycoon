﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class RemoveChoreFromHome
    {
        private readonly IHomeRepository homeRepository;

        public RemoveChoreFromHome(IHomeRepository homeRepository)
        {
            this.homeRepository = homeRepository;
        }

        public void Execute(Home home, Chore chore)
        {
            home.RemoveChore(chore);
            homeRepository.Update(home);
        }
    }
}

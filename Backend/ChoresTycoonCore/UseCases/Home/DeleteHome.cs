﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;

namespace ChoresTycoonCore.UseCases
{
    public class DeleteHome
    {
        private readonly IHomeRepository homeRepository;

        public DeleteHome(IHomeRepository homeRepository)
        {
            this.homeRepository = homeRepository;
        }

        public void Execute(Home home)
        {
            homeRepository.DeleteHomeMembers(home.Id);
            homeRepository.Delete(home.Id);
        }
    }
}


﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class GetMembersFromHome
    {
        private IMemberRepository _memberRepository { get; set; }

        public GetMembersFromHome(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }

        public Collection<Member> Execute(Home home)
        {
            return _memberRepository.GetAllMembersFromHome(home);
        }
    }
}

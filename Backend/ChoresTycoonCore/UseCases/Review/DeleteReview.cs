﻿using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class DeleteReview
    {
        private IReviewRepository repository { get; set; }

        public DeleteReview(IReviewRepository repository)
        {
            this.repository = repository;
        }

        public void Execute(Guid reviewId)
        {
            repository.Delete(reviewId);
        }
    }
}

﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class CreateReview
    {
        private IReviewRepository ReviewRepository { get; set; }
        private IChoreRepository ChoreRepository { get; set; }
        private IHomeRepository HomeRepository { get; set; }

        public CreateReview(IReviewRepository reviewRepository, IChoreRepository choreRepository, IHomeRepository homeRepository)
        {
            ReviewRepository = reviewRepository;
            ChoreRepository = choreRepository;
            HomeRepository = homeRepository;
        }

        public void Execute(Chore chore, Review review)
        {
            Chore? retrievedChore = ChoreRepository.Get(review.ChoreId);
            if (retrievedChore == null)
                throw new Exception("Cannot review an non existent chore.");

            if (!retrievedChore.Completed)
                throw new Exception("Cannot review an uncompleted chore.");

            if (!HomeRepository.MemberBelongsToHome(retrievedChore.HomeId, review.MemberId))
                throw new Exception("Reviewing member does not belong to home.");

            Collection<Review> choreReviews = ReviewRepository.GetAllReviewsFromChore(retrievedChore);
            Review reviewMadeBySameMember = choreReviews.FirstOrDefault(r => r.MemberId == review.MemberId, null);
            if (reviewMadeBySameMember == null)
            {
                ReviewRepository.Create(review);
                chore.AddReview(review);
                ChoreRepository.Update(chore);
            }
        }
    }
}

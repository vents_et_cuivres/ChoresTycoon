﻿using ChoresTycoonCore.Entities;
using ChoresTycoonCore.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.UseCases
{
    public class ModifyReview
    {
        private IReviewRepository repository { get; set; }

        public ModifyReview(IReviewRepository repository)
        {
            this.repository = repository;
        }

        public void Execute(Review review)
        {
            repository.Update(review);
        }
    }
}

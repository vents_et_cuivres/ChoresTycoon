﻿using ChoresTycoonCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.DataTypes
{
    public struct AccountEmail
    {
        public string Value { get; }

        public AccountEmail(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new InvalidMemberEmailException();

            try
            {
                MailAddress address = new MailAddress(value);
            }
            catch (Exception e)
            {
                throw new InvalidMemberEmailException(e.Message);
            }

            Value = value;
        }

        public static implicit operator AccountEmail(string value) => new(value);
        public static implicit operator string(AccountEmail memberEmail) => memberEmail.Value;
    }
}

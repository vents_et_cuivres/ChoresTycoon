﻿using ChoresTycoonCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.DataTypes
{
    public struct HomeName
    {
        public string Value { get; }

        public HomeName(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new InvalidHomeNameException();
            Value = value;
        }

        public static implicit operator HomeName(string value) => new(value);
        public static implicit operator string(HomeName memberName) => memberName.Value;
    }
}

﻿using ChoresTycoonCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.DataTypes
{
    public struct ChoreName
    {
        public string Value { get; }

        public ChoreName(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new InvalidChoreNameException("Chore name cannot be empty.");
            Value = value;
        }

        public static implicit operator ChoreName(string value) => new(value);
        public static implicit operator string(ChoreName choreName) => choreName.Value;
    }
}

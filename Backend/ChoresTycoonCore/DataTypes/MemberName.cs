﻿using ChoresTycoonCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.DataTypes
{
    public struct MemberName
    {
        public string Value { get; }
        
        public MemberName(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new InvalidMemberNameException();
            Value = value;
        }

        public static implicit operator MemberName(string value) => new(value);
        public static implicit operator string(MemberName memberName) => memberName.Value;
    }
}

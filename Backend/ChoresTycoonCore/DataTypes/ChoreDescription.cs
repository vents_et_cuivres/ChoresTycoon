﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.DataTypes
{
    public struct ChoreDescription
    {
        public string Value { get; }

        public ChoreDescription(string value)
        {
            Value = value;
        }

        public static implicit operator ChoreDescription(string value) => new(value);
        public static implicit operator string(ChoreDescription choreDescription) => choreDescription.Value;
    }
}

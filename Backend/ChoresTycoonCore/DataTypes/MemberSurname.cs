﻿using ChoresTycoonCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.DataTypes
{
    public struct MemberSurname
    {
        public string Value { get; }

        public MemberSurname(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new InvalidMemberSurnameException();
            Value = value;
        }

        public static implicit operator MemberSurname(string value) => new(value);
        public static implicit operator string(MemberSurname memberSurname) => memberSurname.Value;
    }
}

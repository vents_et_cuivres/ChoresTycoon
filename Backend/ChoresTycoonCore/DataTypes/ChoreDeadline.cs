﻿using ChoresTycoonCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.DataTypes
{
    public struct ChoreDeadline
    {
        public DateTime Value { get; }

        public ChoreDeadline(DateTime value)
        {
            if (value < new DateTime(2000, 1, 1))
                throw new InvalidChoreDeadlineException();
            Value = new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second, value.Kind);
        }

        public static implicit operator ChoreDeadline(DateTime value) => new(value);
        public static implicit operator DateTime(ChoreDeadline choreDeadline) => choreDeadline.Value;
    }
}

﻿using ChoresTycoonCore.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Entities
{
    public class Account : ICloneable
    {
        public Guid Id { get; private set; }
        public AccountEmail Email { get; set; }
        public string Password { get; set; } = "";
        public Guid MemberId { get; set; }

        public Account(string email, string password)
        {
            Id = Guid.NewGuid();
            Email = email;
            Password = password;
        }

        public Account(Guid id, AccountEmail email, string password, Guid memberId)
        {
            Id = id;
            Email = email;
            Password = password;
            MemberId = memberId;
        }

        public object Clone()
        {
            var newAccount = new Account(Email, Password);
            newAccount.Id = Id;
            newAccount.MemberId = MemberId;
            return newAccount;
        }
    }
}

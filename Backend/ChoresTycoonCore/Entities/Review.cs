﻿using ChoresTycoonCore.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Entities
{
    public class Review : ICloneable
    {
        #region Attributes
        public Guid Id { get; private set; }
        public Guid ChoreId { get; private set; }
        public Guid MemberId { get; private set; }
        public EChoreReviewScore ReviewScore { get; set; }
        #endregion

        #region Constructors

        public Review(Member reviewMember, Chore chore, EChoreReviewScore reviewScore)
        {
            Id = Guid.NewGuid();
            MemberId = reviewMember.Id;
            ChoreId = chore.Id;
            ReviewScore = reviewScore;
        }

        public Review(Guid id, Guid choreId, Guid memberId, EChoreReviewScore reviewScore)
        {
            Id = id;
            ChoreId = choreId;
            MemberId = memberId;
            ReviewScore = reviewScore;
        }

        public object Clone()
        {
            return new Review(Id, ChoreId, MemberId, ReviewScore);
        }
        #endregion
    }
}

﻿using ChoresTycoonCore.DataTypes;
using ChoresTycoonCore.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Entities
{
    public class Chore : ICloneable
    {
        #region Attributes
        public Guid Id { get; private set; } 
        public bool Completed { get; private set; } = false; 
        public ChoreName Name { get; set; }
        public ChoreDescription Description { get; set; }
        public ChoreDeadline Deadline { get; set; }
        public Guid HomeId { get; private set; }

        private Collection<Member> AssignedMembers = new Collection<Member> { };
        private Collection<Review> Reviews = new Collection<Review> { };

        #endregion

        public Chore(Guid id, bool completed, string name, string description, DateTime deadline, Guid homeId)
        {
            Id = id;
            Completed = completed;
            Name = name; 
            Description = description;
            Deadline = deadline;
            HomeId = homeId;
        }

        public Chore(string name, string description, DateTime deadline, Guid homeId)
        {
            Id = Guid.NewGuid();
            Name = name;
            Description = description;
            Deadline = deadline;
            HomeId = homeId;
        }

        public void SetMembers(Collection<Member> members)
        {
            AssignedMembers = new Collection<Member>(members);
        }

        public Collection<Member> GetAssignedMembers()
        {
            return AssignedMembers;
        }

        public void AddMember(Member member)
        {
            var memberWithSameId = AssignedMembers.FirstOrDefault(m => m.Id == member.Id, null);
            if (memberWithSameId == null)
                AssignedMembers.Add(member);      
        }

        public void RemoveMember(Member member)
        {
            var memberWithSameId = AssignedMembers.FirstOrDefault(m => m.Id == member.Id, null);
            if (memberWithSameId != null)
                AssignedMembers.Remove(memberWithSameId);          
        }

        public void Complete()
        {
            Completed = true;
        }

        public void AddReview(Review review)
        {
            var reviewWithSameMemberOrId = Reviews.FirstOrDefault(r => (r.Id == review.Id || r.MemberId == review.MemberId));
            if (reviewWithSameMemberOrId == null)
                Reviews.Add(review);
        }

        public Collection<Review> GetReviews()
        {
            return Reviews;
        }

        public void SetReviews(Collection<Review> reviews)
        {
            Reviews = new Collection<Review>(reviews);
        }

        public object Clone()
        {
            var newChore = new Chore(Id, Completed, Name, Description, Deadline, HomeId);

            foreach(var member in AssignedMembers)
                newChore.AddMember(member);

            foreach(var review in Reviews)
                newChore.AddReview(review);

            return newChore;
        }

        public double GetAverageReviewScore()
        {
            double totalReviewScore = 0;
            foreach (var review in GetReviews())
            {                
                totalReviewScore += (double)review.ReviewScore;
            }

            return Math.Round(totalReviewScore / GetReviews().Count, 1);
            
        }
    }
}

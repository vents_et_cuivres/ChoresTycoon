﻿using ChoresTycoonCore.DataTypes;
using ChoresTycoonCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Entities
{
    public class Member : ICloneable
    {
        #region Attributes
        public Guid Id { get; private set; }
        public MemberSurname Surname { get; set; }
        public MemberName Name { get; set; }

        #endregion
        public Member() { }
        public Member(string surname, string name)
        {
            Id = Guid.NewGuid();
            Surname = surname;
            Name = name;
        }

        public Member(Guid id, MemberSurname surname, MemberName name)
        {
            Id = id;
            Surname = surname;
            Name = name;
        }

        public object Clone()
        {
            var newMember = new Member();
            newMember.Id = this.Id;
            newMember.Name = this.Name;
            newMember.Surname = this.Surname;
            return newMember;
        }
    }
}

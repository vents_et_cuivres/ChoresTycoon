﻿using ChoresTycoonCore.DataTypes;
using ChoresTycoonCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Entities
{
    public class Home : ICloneable
    {
        #region Attributes
        public Guid Id { get; private set; }
        public HomeName Name { get; set; }

        private Collection<Member> Members = new Collection<Member>() { };
        private Collection<Chore> Chores = new Collection<Chore>() { };

        #endregion

        #region Constructors

        public Home() { }

        public Home(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }

        public Home(Guid guid, string name)
        {
            Id = guid;
            Name = name;
        }

        #endregion

        #region Private methods

        public Collection<Member> GetMembers()
        {
            return new Collection<Member>(Members);
        }

        public void SetMembers(Collection<Member> members)
        {
            Members = new Collection<Member>(members);
        }

        public void SetChores(Collection<Chore> chores)
        {
            Chores = new Collection<Chore>(chores);
        }

        public Collection<Chore> GetChores()
        {
            return new Collection<Chore>(Chores);
        }

        public void AddMember(Member member)
        {
            var memberWithSameId = Members.FirstOrDefault(m => m.Id == member.Id, null);
            if (memberWithSameId == null)
                Members.Add(member);            
        }

        public void RemoveMember(Member member)
        {
            var memberWithSameId = Members.FirstOrDefault(m => m.Id == member.Id, null);
            if (memberWithSameId != null)
                Members.Remove(memberWithSameId);
        }

        public void AddChore(Chore chore)
        {
            var choreWithSameId = Chores.FirstOrDefault(c => c.Id == chore.Id, null);
            if (choreWithSameId == null)
                Chores.Add(chore);
        }

        public void RemoveChore(Chore chore)
        {
            var choreWithSameId = Chores.FirstOrDefault(c => c.Id == chore.Id, null);
            if (choreWithSameId != null)
                Chores.Remove(choreWithSameId);
        }





        #endregion

        public object Clone()
        {
            var newHome = new Home(this.Name);
            newHome.Id = this.Id;
            foreach (var member in Members)
                newHome.AddMember(member);
            foreach (var chore in Chores)
                newHome.AddChore(chore);

            return newHome;
        }
    }
}

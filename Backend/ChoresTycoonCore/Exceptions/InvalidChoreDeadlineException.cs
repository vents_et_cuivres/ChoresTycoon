﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class InvalidChoreDeadlineException : Exception
    {
        public InvalidChoreDeadlineException() { }
        public InvalidChoreDeadlineException(string message) : base(message) { }
    }
}

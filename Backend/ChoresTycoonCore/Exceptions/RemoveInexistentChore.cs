﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class RemoveInexistentChore:Exception
    {
        public RemoveInexistentChore() { }
        public RemoveInexistentChore(string message) : base(message) { }


    }
}

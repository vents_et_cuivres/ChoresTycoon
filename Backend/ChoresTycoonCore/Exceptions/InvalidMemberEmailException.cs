﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class InvalidMemberEmailException : Exception
    {
        public InvalidMemberEmailException() { }
        public InvalidMemberEmailException(string message) : base(message) { }
    }
}

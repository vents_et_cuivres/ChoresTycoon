﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class InvalidHomeNameException : Exception
    {
        public InvalidHomeNameException() { }
        public InvalidHomeNameException(string message) : base(message) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class RemoveInexistentMember : Exception
    {
        public RemoveInexistentMember() { }
        public RemoveInexistentMember(string message) : base(message) { }
    }
}

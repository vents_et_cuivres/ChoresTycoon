﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class InvalidChoreNameException : Exception
    {
        public InvalidChoreNameException() { }
        public InvalidChoreNameException(string message) : base(message) { }
    }
}

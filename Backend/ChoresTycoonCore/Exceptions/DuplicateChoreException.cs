﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class DuplicateChoreException:Exception
    {
        public DuplicateChoreException() { }
        public DuplicateChoreException(string message) : base(message) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class InvalidMemberNameException : Exception
    {
        public InvalidMemberNameException() { }
        public InvalidMemberNameException(string message) : base(message) { }
    }
}

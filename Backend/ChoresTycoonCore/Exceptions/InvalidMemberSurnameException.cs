﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Exceptions
{
    public class InvalidMemberSurnameException : Exception
    {
        public InvalidMemberSurnameException() { }
        public InvalidMemberSurnameException(string message) : base(message) { }
    }
}

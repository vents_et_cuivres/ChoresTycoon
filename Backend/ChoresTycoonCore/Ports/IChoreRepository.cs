﻿using ChoresTycoonCore.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Ports
{
    public interface IChoreRepository
    {
        public void Create(Chore chore);
        public IReadOnlyCollection<Chore> GetAll();
        public Chore? Get(Guid choreId);
        public void Update(Chore chore);
        public void Delete(Guid choreID);
        public Collection<Chore> GetAllChoresFromHome(Home home);
        public Collection<Chore> GetAllChoresFromMember(Guid memberId);
    }
}

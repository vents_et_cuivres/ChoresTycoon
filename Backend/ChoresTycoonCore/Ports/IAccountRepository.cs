﻿using ChoresTycoonCore.Entities;

namespace ChoresTycoonCore.Ports
{
    public interface IAccountRepository
    {
        public void Create(Account account);
        public IReadOnlyCollection<Account> GetAll();
        public Account? Get(Guid accountId);
        public void Update(Account account);
        public void Delete(Guid accountId);
        public Account? GetAccountByEmail(string email);
    }
}
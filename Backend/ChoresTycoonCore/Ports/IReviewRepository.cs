﻿using ChoresTycoonCore.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Ports
{
    public interface IReviewRepository
    {
        public void Create(Review review);
        public IReadOnlyCollection<Review> GetAll();
        public Review? Get(Guid reviewId);
        public void Update(Review review);
        public void Delete(Guid reviewId);
        public Collection<Review> GetAllReviewsFromChore(Chore chore);
    }
}

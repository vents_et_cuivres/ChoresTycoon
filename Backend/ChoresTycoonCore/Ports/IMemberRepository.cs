﻿using ChoresTycoonCore.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChoresTycoonCore.Ports
{
    public interface IMemberRepository
    {
        public void Create(Member member);
        public IReadOnlyCollection<Member> GetAll();
        public Member? Get(Guid memberId);
        public void Update(Member member);
        public void Delete(Guid memberId);
        public Collection<Member> GetAllMembersFromHome(Home home);
        public Collection<Member> GetAllMembersAssignedToChore(Chore chore);
        public void AssignMemberToChore(Guid choreId, Guid memberId);
        public void RemoveMemberFromChore(Guid choreId, Guid memberId);
    }
}

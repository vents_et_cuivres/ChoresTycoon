﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoresTycoonCore.Entities;

namespace ChoresTycoonCore.Ports
{
    public interface IHomeRepository
    {
        public void Create(Home Home);
        public IReadOnlyCollection<Home> GetAll();
        public Home? Get(Guid homeId);
        public void Update(Home home);
        public void Delete(Guid homeId);
        public void AddMemberToHome(Home home, Member member);
        public void RemoveMemberFromHome(Home home, Member member);
        public void DeleteHomeMembers(Guid id);
        public bool MemberBelongsToHome(Guid homeId, Guid memberId);
        public Collection<Home> GetAllHomesFromMember(Guid memberId);
    }
}

import { UpdateChoreInformationModule } from './update-chore-information/update-chore-information.module';
import { ChoreDetailsModule } from './chore-details/chore-details.module';
import { CreateHomeModule } from './create-home/create-home.module';
import { ProtectedRoutingModule } from './protected-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeModule } from './home/home.module';
import { HomeDetailsModule } from './home-details/home-details.module';
import { AddChoreModule } from './add-chore/add-chore.module';
import { UpdateInformationModule } from './update-member-information/update-information.module';
import { UserListModule } from './user-list/user-list.module';

@NgModule({
  declarations: [
  ],
  imports: [
    SharedModule,
    ProtectedRoutingModule,
    HomeModule,
    CreateHomeModule,
    HomeDetailsModule,
    AddChoreModule,
    ChoreDetailsModule,
    UpdateInformationModule,
    UpdateChoreInformationModule,
    UserListModule
  ]
})
export class ProtectedModule { }

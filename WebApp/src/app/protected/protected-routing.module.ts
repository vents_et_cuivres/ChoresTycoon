import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "",
    loadChildren: () => import("./home/home.module")
    .then(mod => mod.HomeModule)
  },
  { path: "",
  loadChildren: () => import("./create-home/create-home.module")
  .then(mod => mod.CreateHomeModule)
  },
  { path: "",
  loadChildren: () => import("./home-details/home-details.module")
  .then(mod => mod.HomeDetailsModule)
  },
  { path: "",
  loadChildren: () => import("./add-chore/add-chore.module")
  .then(mod => mod.AddChoreModule)
  },
  { path: "",
  loadChildren: () => import("./chore-details/chore-details.module")
  .then(mod => mod.ChoreDetailsModule)
  },
  { path: "",
  loadChildren: () => import("./update-member-information/update-information.module")
  .then(mod => mod.UpdateInformationModule)
  },
  { path: "",
  loadChildren: () => import("./update-chore-information/update-chore-information.module")
  .then(mod => mod.UpdateChoreInformationModule)
  },
  { path: "",
  loadChildren: () => import("./update-home-information/update-home-information.module")
  .then(mod => mod.UpdateHomeInformationModule)
  },
  { path: "",
  loadChildren: () => import("./user-list/user-list.module")
  .then(mod => mod.UserListModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }
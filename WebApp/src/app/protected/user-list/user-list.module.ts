import { NgModule } from '@angular/core';

import { UserListRoutingModule } from './user-list-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserListComponent } from './user-list/user-list.component';


@NgModule({
  declarations: [
    UserListComponent
  ],
  imports: [
    SharedModule,
    UserListRoutingModule
  ]
})
export class UserListModule { }

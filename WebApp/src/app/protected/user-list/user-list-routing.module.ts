import { UserListComponent } from './user-list/user-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsConnected } from 'src/app/core/guards/is-connected.guard';

const routes: Routes = [
  {path: "add-member/:id", component: UserListComponent, canActivate: [IsConnected]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserListRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from 'src/app/core/services/home.service';
import { MemberService } from 'src/app/core/services/member.service';
import { Home } from 'src/app/shared/models/Home';
import { Member } from 'src/app/shared/models/Member';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  home?: Home;
  allMembers?: Array<Member>;

  constructor(private route: ActivatedRoute, private router : Router, private homeService: HomeService, private memberService: MemberService) { }

  ngOnInit(): void {
    this.allMembers = new Array<Member>();

    this.homeService.getHome(this.route.snapshot.params['id']).subscribe(res => {
      this.home = res;
      this.memberService.searchMembers(this.home?.id!).subscribe(res => {

        res.forEach(member => {
          let memberAlreadyInHome: boolean = false;
          this.home?.members.forEach(memberInHome => {
            if (member.id === memberInHome.id) {
              memberAlreadyInHome = true;
            }
          });
          if (!memberAlreadyInHome)
            this.allMembers?.push(member);
        });
      });
    });
  }

  addMemberToHome(memberId: string): void {
    this.homeService.addMemberToHome(this.home?.id!, memberId).subscribe(
      data => {
        this.router.navigate(["/"]).then(() => {
          this.router.navigate([`/home-details/${this.home?.id!}`]);
        });
      },
      error => {
        console.log(error);
      }
    );
  }

}

import { ChoreDetailsComponent } from './chore-details/chore-details.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsConnected } from 'src/app/core/guards/is-connected.guard';

const routes: Routes = [
  {path: "chore-details/:id", component: ChoreDetailsComponent, canActivate: [IsConnected]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChoreDetailsRoutingModule { }

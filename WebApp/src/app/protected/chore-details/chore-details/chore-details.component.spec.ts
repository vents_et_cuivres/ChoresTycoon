import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoreDetailsComponent } from './chore-details.component';

describe('ChoreDetailsComponent', () => {
  let component: ChoreDetailsComponent;
  let fixture: ComponentFixture<ChoreDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoreDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoreDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

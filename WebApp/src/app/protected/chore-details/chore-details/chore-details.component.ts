import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ChoreService } from 'src/app/core/services/chore.service';
import { MemberService } from 'src/app/core/services/member.service';
import { Chore } from 'src/app/shared/models/Chore';
import { Member } from 'src/app/shared/models/Member';

@Component({
  selector: 'app-chore-details',
  templateUrl: './chore-details.component.html',
  styleUrls: ['./chore-details.component.css']
})
export class ChoreDetailsComponent implements OnInit {

  chore$?: Chore;
  reviewChoreForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private choreService: ChoreService, private memberService: MemberService, private router: Router) { }

  ngOnInit(): void {
    this.choreService.getChore(this.route.snapshot.params['id']).subscribe(res => {
      this.chore$ = res;
    });

    this.reviewChoreForm = this.formBuilder.group({
      reviewScore: []
    });
  }

  public assignMeToChore(): void {
    this.memberService.getConnectedUserMember().subscribe(res => {
      let currentMember: Member = res;
      if (this.chore$ != undefined) {
        this.choreService.assignMemberToChore(this.chore$.id, currentMember.id).subscribe(
          data => {
            this.router.navigate(["/"]).then(() => {
              this.router.navigate([`/chore-details/${this.chore$!.id}`]);
            });
          },
          error => {
            console.log(error);
          }
        )
      }
    });
  }

  public completeChore(): void {
    this.choreService.postCompleteChore(this.chore$!.id).subscribe(
      data => {
        this.router.navigate(["/"]).then(() => {
          this.router.navigate([`/chore-details/${this.chore$!.id}`]);
        });
      },
      error => {
        console.log(error);
      }
    );
  }

  public reviewChore(): void {
    let reviewScore: number = parseInt(this.reviewChoreForm.value.reviewScore);
    this.memberService.getConnectedUserMember().subscribe(res => {
      let currentMember: Member = res;
      this.choreService.postCreateReview(this.chore$!.id, currentMember.id, reviewScore).subscribe(
        data => {
          this.router.navigate(["/"]).then(() => {
            this.router.navigate([`/chore-details/${this.chore$!.id}`]);
          });
        },
        error => {
          console.log(error);
        }
      )

    });
  }

}

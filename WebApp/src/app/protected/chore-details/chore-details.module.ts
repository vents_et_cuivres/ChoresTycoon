import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChoreDetailsRoutingModule } from './chore-details-routing.module';
import { ChoreDetailsComponent } from './chore-details/chore-details.component';


@NgModule({
  declarations: [
    ChoreDetailsComponent
  ],
  imports: [
    SharedModule,
    ChoreDetailsRoutingModule
  ]
})
export class ChoreDetailsModule { }

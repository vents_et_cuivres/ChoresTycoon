import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';

import { AddChoreRoutingModule } from './add-chore-routing.module';
import { AddChoreComponent } from './add-chore/add-chore.component';


@NgModule({
  declarations: [
    AddChoreComponent
  ],
  imports: [
    SharedModule,
    AddChoreRoutingModule
  ]
})
export class AddChoreModule { }

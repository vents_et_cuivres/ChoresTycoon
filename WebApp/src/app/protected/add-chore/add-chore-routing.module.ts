import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsConnected } from 'src/app/core/guards/is-connected.guard';
import { AddChoreComponent } from './add-chore/add-chore.component';

const routes: Routes = [
  {path: "add-chore/:id", component: AddChoreComponent, canActivate: [IsConnected]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddChoreRoutingModule { }

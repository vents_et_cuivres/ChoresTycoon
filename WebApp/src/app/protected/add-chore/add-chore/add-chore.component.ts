import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ChoreService } from 'src/app/core/services/chore.service';
import { Account } from 'src/app/shared/models/Account';
import { Chore } from 'src/app/shared/models/Chore';
import { AuthState } from 'src/app/shared/stores/states/auth-state';

@Component({
  selector: 'app-add-chore',
  templateUrl: './add-chore.component.html',
  styleUrls: ['./add-chore.component.css']
})
export class AddChoreComponent implements OnInit {
  @Select(AuthState.isConnected) isConnected$?: Observable<boolean>;
  
  createChoreForm : FormGroup;
  homeId: string;
  
  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private choreService: ChoreService, private router : Router) {
    this.homeId = this.route.snapshot.params['id'],
    this.createChoreForm = this.formBuilder.group({
      name: [],
      description: [],
    }); 
  }

  get form() : { [key: string]: AbstractControl }
  {
    return this.createChoreForm.controls;
  }

  ngOnInit(): void {
  }

  onSubmit() : void {
    const chore : Chore = this.createChoreForm.value;
    chore.homeId = this.homeId;

    this.choreService.createChore(chore)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([`/home-details/${chore.homeId}`]);
        },
        error => {
          console.log(error);
        }
      )
  }
}

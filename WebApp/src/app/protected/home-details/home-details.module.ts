import { NgModule } from '@angular/core';

import { HomeDetailsRoutingModule } from './home-details-routing.module';
import { HomeDetailsComponent } from './home-details/home-details.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeDetailsChoresComponent } from './home-details-chores/home-details-chores.component';
import { HomeDetailsMembersComponent } from './home-details-members/home-details-members.component';


@NgModule({
  declarations: [
    HomeDetailsComponent,
    HomeDetailsChoresComponent,
    HomeDetailsMembersComponent
  ],
  imports: [
    SharedModule,
    HomeDetailsRoutingModule
  ]
})
export class HomeDetailsModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDetailsMembersComponent } from './home-details-members.component';

describe('HomeDetailsMembersComponent', () => {
  let component: HomeDetailsMembersComponent;
  let fixture: ComponentFixture<HomeDetailsMembersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeDetailsMembersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDetailsMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

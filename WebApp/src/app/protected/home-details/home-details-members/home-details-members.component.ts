import { Component, Input, OnInit } from '@angular/core';
import { Member } from 'src/app/shared/models/Member';

@Component({
  selector: 'app-home-details-members',
  templateUrl: './home-details-members.component.html',
  styleUrls: ['./home-details-members.component.css']
})
export class HomeDetailsMembersComponent implements OnInit {

  @Input() member ?: Member;

  constructor() { }

  ngOnInit(): void {
  }

}

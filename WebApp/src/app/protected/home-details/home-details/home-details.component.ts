import { MemberService } from './../../../core/services/member.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ChoreService } from 'src/app/core/services/chore.service';
import { HomeService } from 'src/app/core/services/home.service';
import { Chore } from 'src/app/shared/models/Chore';
import { Home } from 'src/app/shared/models/Home';
import { Member } from 'src/app/shared/models/Member';

@Component({
  selector: 'app-home-details',
  templateUrl: './home-details.component.html',
  styleUrls: ['./home-details.component.css']
})
export class HomeDetailsComponent implements OnInit {

  home$ ?: Observable<Home | undefined>;

  choreList$ ?: Observable<Array<Chore>>;

  memberList$ ?: Observable<Array<Member>>;

  constructor(private route: ActivatedRoute, private homeService: HomeService, private choreService: ChoreService, private memberService: MemberService) { }

  ngOnInit(): void {
    this.home$ = this.homeService.getHome(this.route.snapshot.params['id']);
    this.choreList$ = this.choreService.getHomeChores(this.route.snapshot.params['id']);
    this.memberList$ = this.memberService.getHomeMembers(this.route.snapshot.params['id']);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDetailsChoresComponent } from './home-details-chores.component';

describe('HomeDetailsChoresComponent', () => {
  let component: HomeDetailsChoresComponent;
  let fixture: ComponentFixture<HomeDetailsChoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeDetailsChoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDetailsChoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

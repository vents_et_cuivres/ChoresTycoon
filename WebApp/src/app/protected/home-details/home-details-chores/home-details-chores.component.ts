import { Component, Input, OnInit } from '@angular/core';
import { ChoreService } from 'src/app/core/services/chore.service';
import { Chore } from 'src/app/shared/models/Chore';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-details-chores',
  templateUrl: './home-details-chores.component.html',
  styleUrls: ['./home-details-chores.component.css']
})
export class HomeDetailsChoresComponent implements OnInit {

  @Input() chore ?: Chore;
  
  constructor(private choreService: ChoreService, private router : Router) { }

  ngOnInit(): void {
  }

  deleteChore() : void {
    this.choreService.deleteChore(this.chore!.id)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/"]).then(() => {
            this.router.navigate([`/home-details/${this.chore!.homeId}`]);
          });
        },
        error => {
          console.log(error);
        }
      )
  }
}

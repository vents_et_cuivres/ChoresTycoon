import { Home } from '../../../shared/models/Home';
import { Component, Input, OnInit } from '@angular/core';
import { HomeService } from 'src/app/core/services/home.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-item',
  templateUrl: './home-item.component.html',
  styleUrls: ['./home-item.component.css']
})
export class HomeItemComponent implements OnInit {

  @Input() home ?: Home;

  constructor(private homeService: HomeService, private router : Router) { }

  ngOnInit(): void {
  }

  public onHomeDelete(homeId: string) : void {
    this.homeService.deleteHome(homeId)
    .pipe(first())
    .subscribe(
      data => {
        this.router.navigate(["/"]).then(() => {
          this.router.navigate(["/home"]);
        });
      },
      error => {
        console.log(error);
      }
    )
  }

}

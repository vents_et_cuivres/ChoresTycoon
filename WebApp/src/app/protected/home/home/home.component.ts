import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HomeService } from 'src/app/core/services/home.service';
import { MemberService } from 'src/app/core/services/member.service';
import { Home } from 'src/app/shared/models/Home';
import { Member } from 'src/app/shared/models/Member';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  homes$!: Observable<Array<Home>>; 
  currentMember!: Member;

  constructor(private homeService: HomeService, private memberService: MemberService) { }

  ngOnInit(): void {
    this.memberService.getConnectedUserMember().subscribe(
      (res) => {
        this.currentMember = res;
        this.homes$ = this.homeService.getHomesFromMember(this.currentMember.id);
      }
    );    
  }

}

import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsConnected } from 'src/app/core/guards/is-connected.guard';

const routes: Routes = [
  { path: "home", component: HomeComponent, canActivate: [IsConnected]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
import { UpdateHomeInformationRoutingModule } from './update-home-information-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateHomeInformationComponent } from './update-home-information/update-home-information.component';



@NgModule({
  declarations: [
    UpdateHomeInformationComponent
  ],
  imports: [
    SharedModule,
    UpdateHomeInformationRoutingModule
  ]
})
export class UpdateHomeInformationModule { }

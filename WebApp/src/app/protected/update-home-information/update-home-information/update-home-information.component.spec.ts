import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateHomeInformationComponent } from './update-home-information.component';

describe('UpdateHomeInformationComponent', () => {
  let component: UpdateHomeInformationComponent;
  let fixture: ComponentFixture<UpdateHomeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateHomeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateHomeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

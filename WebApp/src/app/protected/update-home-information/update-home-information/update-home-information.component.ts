import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { HomeService } from 'src/app/core/services/home.service';
import { Account } from 'src/app/shared/models/Account';
import { AuthState } from 'src/app/shared/stores/states/auth-state';

@Component({
  selector: 'app-update-home-information',
  templateUrl: './update-home-information.component.html',
  styleUrls: ['./update-home-information.component.css']
})
export class UpdateHomeInformationComponent implements OnInit {

  @Select(AuthState.isConnected) isConnected$?: Observable<boolean>;

  updateHomeForm : FormGroup;
  
  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private homeService: HomeService, private router : Router) {
    this.updateHomeForm = this.formBuilder.group({
      name: []
    }); 
  }

  get form() : { [key: string]: AbstractControl }
  {
    return this.updateHomeForm.controls;
  }

  ngOnInit(): void {
  }

  onSubmit() : void {
    const homeName : string = this.updateHomeForm.value.name;

    this.homeService.updateHome(this.route.snapshot.params['id'], homeName)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/home"]);
        },
        error => {
          console.log(error);
        }
      )
  }

}

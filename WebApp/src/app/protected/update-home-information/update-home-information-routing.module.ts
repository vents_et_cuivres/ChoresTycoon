import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsConnected } from 'src/app/core/guards/is-connected.guard';
import { UpdateHomeInformationComponent } from './update-home-information/update-home-information.component';

const routes: Routes = [
  {path: "update-home-information/:id", component: UpdateHomeInformationComponent, canActivate: [IsConnected]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateHomeInformationRoutingModule { }

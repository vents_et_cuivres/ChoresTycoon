import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsConnected } from 'src/app/core/guards/is-connected.guard';
import { UpdateChoreInformationComponent } from './update-chore-information/update-chore-information.component';

const routes: Routes = [
  {path: "update-chore-information", component: UpdateChoreInformationComponent, canActivate: [IsConnected]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateChoreInformationRoutingModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateChoreInformationComponent } from './update-chore-information.component';

describe('UpdateChoreInformationComponent', () => {
  let component: UpdateChoreInformationComponent;
  let fixture: ComponentFixture<UpdateChoreInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateChoreInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateChoreInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

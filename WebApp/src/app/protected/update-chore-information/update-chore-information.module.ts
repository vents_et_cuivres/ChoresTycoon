import { UpdateChoreInformationRoutingModule } from './update-chore-information-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateChoreInformationComponent } from './update-chore-information/update-chore-information.component';



@NgModule({
  declarations: [
    UpdateChoreInformationComponent
  ],
  imports: [
    SharedModule,
    UpdateChoreInformationRoutingModule
  ]
})
export class UpdateChoreInformationModule { }

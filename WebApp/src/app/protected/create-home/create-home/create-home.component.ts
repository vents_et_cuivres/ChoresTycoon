import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { HomeService } from 'src/app/core/services/home.service';
import { Account } from 'src/app/shared/models/Account';
import { AuthState } from 'src/app/shared/stores/states/auth-state';

@Component({
  selector: 'app-create-home',
  templateUrl: './create-home.component.html',
  styleUrls: ['./create-home.component.css']
})
export class CreateHomeComponent implements OnInit {

  @Select(AuthState.isConnected) isConnected$?: Observable<boolean>;

  homeCreationForm : FormGroup;
  
  constructor(private formBuilder: FormBuilder, private homeService: HomeService, private router : Router) {
    this.homeCreationForm = this.formBuilder.group({
      homeName: []
    }); 
  }

  get form() : { [key: string]: AbstractControl }
  {
    return this.homeCreationForm.controls;
  }

  ngOnInit(): void {
  }

  onSubmit() : void {
    let homeName : string = this.homeCreationForm.value.homeName;

    this.homeService.createHome(homeName)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/home"]);
        },
        error => {
          console.log(error);
        }
      )
  }

}

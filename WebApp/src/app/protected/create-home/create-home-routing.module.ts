import { CreateHomeComponent } from './create-home/create-home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsConnected } from 'src/app/core/guards/is-connected.guard';

const routes: Routes = [
  { path: "create-home", component: CreateHomeComponent, canActivate: [IsConnected]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateHomeRoutingModule { }

import { NgModule } from '@angular/core';

import { CreateHomeRoutingModule } from './create-home-routing.module';
import { CreateHomeComponent } from './create-home/create-home.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    CreateHomeComponent
  ],
  imports: [
    SharedModule,
    CreateHomeRoutingModule
  ]
})
export class CreateHomeModule { }

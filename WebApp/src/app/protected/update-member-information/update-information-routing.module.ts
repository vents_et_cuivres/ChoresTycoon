import { UpdateInformationComponent } from './update-information/update-information.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsConnected } from 'src/app/core/guards/is-connected.guard';

const routes: Routes = [
  {path: "update-information", component: UpdateInformationComponent, canActivate: [IsConnected]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateInformationRoutingModule { }

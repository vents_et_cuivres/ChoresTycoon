import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { MemberService } from 'src/app/core/services/member.service';
import { Account } from 'src/app/shared/models/Account';
import { Member } from 'src/app/shared/models/Member';
import { AuthState } from 'src/app/shared/stores/states/auth-state';

@Component({
  selector: 'app-update-information',
  templateUrl: './update-information.component.html',
  styleUrls: ['./update-information.component.css']
})
export class UpdateInformationComponent implements OnInit {

  updateMemberForm!: FormGroup;
  updateAccountForm!: FormGroup;
  currentMember!: Member;

  constructor(private formBuilder: FormBuilder, private memberService: MemberService, private authenticationService: AuthenticationService, private router : Router)
  {
    this.memberService.getConnectedUserMember().subscribe(
      (res) => {
        this.currentMember = res;
        this.updateMemberForm = this.formBuilder.group({
          name: [res.name],
          surname: [res.surname],
        }); 
    
        this.updateAccountForm = this.formBuilder.group({
          email: [],
          password: [],
        });
      }
    );  
   }

  ngOnInit(): void {

  }

  onUpdateAccountSubmit() : void {
    let email : string = "";
    if (this.updateAccountForm.value.email != "") {
      email = this.updateAccountForm.value.email;
    }
    
    let password : string = "";
    if (this.updateAccountForm.value.password != "") {
      password = this.updateAccountForm.value.password;
    }

    this.authenticationService.updateAccount(email, password)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/home"]);
        },
        error => {
          console.log(error);
        }
      )
  }

  onUpdateMemberSubmit() : void {
    let modifiedMember = this.currentMember;
    modifiedMember.name = this.updateMemberForm.value.name;
    modifiedMember.surname = this.updateMemberForm.value.surname;

    this.memberService.updateMember(modifiedMember)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/home"]);
        },
        error => {
          console.log(error);
        }
      )
  }

}

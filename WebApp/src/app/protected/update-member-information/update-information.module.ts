import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpdateInformationRoutingModule } from './update-information-routing.module';
import { UpdateInformationComponent } from './update-information/update-information.component';


@NgModule({
  declarations: [
    UpdateInformationComponent
  ],
  imports: [
    SharedModule,
    UpdateInformationRoutingModule
  ]
})
export class UpdateInformationModule { }

import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Account } from 'src/app/shared/models/Account';
import { AuthState } from 'src/app/shared/stores/states/auth-state';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  @Select(AuthState.isConnected) isConnected$?: Observable<boolean>;

  loginForm : FormGroup;
  
  constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private router : Router) {
    this.loginForm = this.formBuilder.group({
      email: [],
      password: [],
    }); 
  }

  get form() : { [key: string]: AbstractControl }
  {
    return this.loginForm.controls;
  }

  ngOnInit(): void {
  }

  onSubmit() : void {
    const account : Account = this.loginForm.value;

    this.authenticationService.signin(account)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/home"]);
        },
        error => {
          console.log(error);
        }
      )
  }
}

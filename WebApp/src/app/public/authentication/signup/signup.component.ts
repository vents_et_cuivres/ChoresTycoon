import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Account } from 'src/app/shared/models/Account';
import { AuthState } from 'src/app/shared/stores/states/auth-state';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm : FormGroup;
  
  constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private router : Router) {
    this.signupForm = this.formBuilder.group({
      email: [],
      password: [],
    }); 
  }

  get form() : { [key: string]: AbstractControl }
  {
    return this.signupForm.controls;
  }

  ngOnInit(): void {
  }

  onSubmit() : void {
    const account : Account = this.signupForm.value;

    this.authenticationService.signup(account)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/signin"]);
        },
        error => {
          console.log(error);
        }
      )
  }
}

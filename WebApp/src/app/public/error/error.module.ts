import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { ErrorComponent } from './error/error.component';



@NgModule({
  declarations: [
    ErrorComponent
  ],
  imports: [
    SharedModule
  ]
})
export class ErrorModule { }

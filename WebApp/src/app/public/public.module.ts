import { WelcomeModule } from './welcome/welcome.module';
import { ErrorModule } from './error/error.module';
import { HomeModule } from '../protected/home/home.module';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { PublicRoutingModule } from './public-routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    SharedModule,
    PublicRoutingModule,
    AuthenticationModule,
    ErrorModule,
    WelcomeModule
  ]
})
export class PublicModule { }

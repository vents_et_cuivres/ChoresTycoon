import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './public/error/error/error.component';
import { WelcomeComponent } from './public/welcome/welcome/welcome.component';

const routes: Routes = [
  { path: "", component: WelcomeComponent},
  {
    path: "",
    loadChildren: () =>
      import("./public/public.module").then(
        mod => mod.PublicModule
      )
  },
  {
    path: "",
    loadChildren: () =>
      import("./protected/protected.module").then(
        mod => mod.ProtectedModule
      )
  },
  { path: "404-not-found", component: ErrorComponent},
  { path: "**", redirectTo: "404-not-found"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

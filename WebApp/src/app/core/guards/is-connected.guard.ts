import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { AuthState } from "src/app/shared/stores/states/auth-state";

@Injectable()
export class IsConnected implements CanActivate {

    constructor(private store: Store, private router: Router) { }

    canActivate(): Observable<boolean> { 
        return this.store.select(AuthState.isConnected).pipe(
            map((isConnected: boolean) => {
                if (!isConnected)
                    this.router.navigateByUrl('/');

                return isConnected;
            })
        );
    }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from 'src/app/shared/models/Member';
import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class MemberService extends HttpRequestService {

  constructor(protected override httpClient: HttpClient) { super(httpClient); }

  public searchMembers(homeId: string): Observable<Array<Member>> {
    return this.get<Array<Member>>("/api/member/search");
  }

  public getHomeMembers(homeId: string): Observable<Array<Member>> {
    const body = {homeId: homeId};
    return this.post<Array<Member>>("/api/home/getAllMembersFromHome", body);
  }

  public updateMember(member: Member) : Observable<string> {
    const body = {memberId: member.id, name: member.name, surname: member.surname};
    return this.post<string>("/api/member/modify", body);
  }
  
  public deleteMember(memberId: string) : Observable<string>{
    const body = {memberId: memberId};
    return this.post<string>("/api/member/delete", body);
  }
  
  public getConnectedUserMember() : Observable<Member> {
    return this.get<Member>("/api/account/getMember");
  }
}

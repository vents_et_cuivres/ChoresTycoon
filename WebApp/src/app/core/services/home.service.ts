import { Home } from './../../shared/models/Home';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService extends HttpRequestService {

  constructor(protected override httpClient: HttpClient) {
    super(httpClient)
  }

  public getHome(homeId: string) : Observable<Home | undefined> {
    const body = {homeId: homeId};
    return this.post<Home>("/api/home/readHome", body);
  }

  public createHome(homeName: string) : Observable<string> {
    const body = {name: homeName};
    return this.post<string>("/api/home/create", body);
  }

  public updateHome(homeId: string, homeName: string) : Observable<string> {
    const body = {homeId: homeId, name: homeName};
    return this.post<string>("/api/home/modify", body);
  }

  public deleteHome(homeId: string) : Observable<string> {
    const body = {homeId: homeId};
    return this.post<string>("/api/home/delete", body);
  }

  public addMemberToHome(homeId: string, memberId: string) : Observable<string>  {
    const body = {homeId: homeId, memberId: memberId};
    return this.post<string>("/api/home/addMemberToHome", body);
  }

  public removeMemberFromHome(homeId: string, memberId: string) : Observable<string>  {
    const body = {homeId: homeId, memberId: memberId};
    return this.post<string>("/api/home/removeMemberFromHome", body);
  }

  public getHomesFromMember(memberId: string) : Observable<Array<Home>> {
    const body = {memberId: memberId};
    return this.post<Array<Home>>("/api/member/getHomes", body);
  }
}

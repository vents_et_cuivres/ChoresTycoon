import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { HttpRequestService } from './http-request.service';

import { Chore} from './../../shared/models/Chore';
import { Member } from 'src/app/shared/models/Member';

@Injectable({
  providedIn: 'root'
})
export class ChoreService extends HttpRequestService {

  constructor(protected override httpClient: HttpClient) {
    super(httpClient);
  }

  public getHomeChores(homeId: string): Observable<Array<Chore>> {
    const body = {homeId: homeId};
    return this.post<Array<Chore>>("/api/home/getAllChoresFromHome", body);
  }

  public getChore(choreId: string): Observable<Chore | undefined> {
    const body = {choreId: choreId};
    return this.post<Chore>("/api/chore/readChore", body);
  }

  public createChore(chore: Chore): Observable<string> {
    const body = {name: chore.name, description: chore.description, deadline: chore.deadline, homeId: chore.homeId};
    return this.post<string>("/api/chore/create", body);
  }

  public deleteChore(choreId: string) : Observable<string> {
    const body = {choreId: choreId};
    return this.post<string>("/api/chore/delete", body);
  }

  public updateChore(chore: Chore) : Observable<string> {
    const body = {choreId: chore.id, name: chore.name, description: chore.description, deadline: chore.deadline};
    return this.post<string>("/api/chore/update", body);
  }

  public assignMemberToChore(choreId : string, memberId: string) : Observable<string>
  {
    const body = {choreId: choreId, memberId: memberId};
    return this.post<string>("/api/chore/assignMemberToChore", body);
  }

  public removeMemberFromChore(choreId : string, memberId: string) : Observable<string>
  {
    const body = {choreId: choreId, memberId: memberId};
    return this.post<string>("/api/chore/removeMemberFromChore", body);
  }
  
  public getChoresFromMember(memberId: string) : Observable<Array<Chore>> {
    const body = {memberId: memberId};
    return this.post<Array<Chore>>("/api/chore/getChores", body);
  }

  public postCompleteChore(choreId: string) : Observable<string> {
    const body = {choreId: choreId};
    return this.post<string>("/api/chore/complete", body);
  }

  public postCreateReview(choreId: string, memberId: string, reviewScore: number) : Observable<string> {
    const body = {choreId: choreId, memberId: memberId, reviewScore: reviewScore};
    return this.post<string>("/api/review/create", body);
  }

}

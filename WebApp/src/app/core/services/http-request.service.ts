import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  baseUrl : string = "http://localhost:5148";

  constructor(protected httpClient : HttpClient) { }

  get<T> (route : string) : Observable<T> {
    return this.httpClient.get<T>(`${this.baseUrl}${route}`);
  }

  post<T> (route : string, body : Object) : Observable<T> {
    const httpOptions = { headers: new HttpHeaders({ "Content-Type" : "application/json"})};
    return this.httpClient.post<T>(`${this.baseUrl}${route}`, body, httpOptions);
  }

  put<T> (route : string, body : Object) : Observable<T> {
    const httpOptions = { headers: new HttpHeaders({ "Content-Type" : "application/json"})};
    return this.httpClient.put<T>(`${this.baseUrl}${route}`, body, httpOptions);
  }

  delete<T> (route : string) : Observable<T> {
    return this.httpClient.delete<T>(`${this.baseUrl}${route}`);
  }
}

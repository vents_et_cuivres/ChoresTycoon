import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Account } from 'src/app/shared/models/Account';
import { Signout } from 'src/app/shared/stores/actions/auth-action';
import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService extends HttpRequestService {

  tokenParse : string = "";

  constructor(protected override httpClient : HttpClient, private store: Store) { 
    super(httpClient);
  }

  public signup(account : Account) : Observable<Account> {
    const body = {email: account.email, password: account.password};
    return this.post<Account>("/api/signup", body);
  }

  public signin(account : Account) : Observable<Account> {
    const body = {email: account.email, password: account.password};
    return this.post<Account>("/api/login", body);
  }

  public updateAccount(email: string, password: string) : Observable<Account> {
    let body = {};
    if (email != "" && password != "") {
      body = {email: email, password: password};
    }
    if (email === "" && password != "") {
      body = {password: password};
    }
    if (email != "" && password === "") {
      body = {email: email};
    }
      
    return this.post<Account>("/api/account/update", body);
  }

  public logout() : void {
    this.store.dispatch(new Signout());
  }
}
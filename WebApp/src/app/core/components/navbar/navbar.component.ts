import { Component, OnInit } from '@angular/core';
import { Event, Router, RouterEvent } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AuthState } from 'src/app/shared/stores/states/auth-state';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Select(AuthState.isConnected) isConnected$?: Observable<Boolean>;
  
  currentRoute: string = "";

  constructor(private router: Router, private authentificationService : AuthenticationService) { 
    router.events.pipe(
      filter((e: Event): e is RouterEvent => e instanceof RouterEvent)
    ).subscribe((e: RouterEvent) => {
      this.currentRoute = e.url;
    });
  }

  ngOnInit(): void {
  }

  signout() {
    this.authentificationService.logout();
    this.router.navigate(['/']);
  }

}

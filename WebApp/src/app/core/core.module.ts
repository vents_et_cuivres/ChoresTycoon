import { RouterModule } from '@angular/router';
import { PublicModule } from './../public/public.module';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProtectedModule } from '../protected/protected.module';



@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    CommonModule,
    PublicModule,
    ProtectedModule,
    RouterModule
  ],
  exports: [
    HttpClientModule,
    NavbarComponent,
  ]
})
export class CoreModule { 

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if(parentModule) {
      throw new Error('CoreModule is already loaded.');
    }
  }
}

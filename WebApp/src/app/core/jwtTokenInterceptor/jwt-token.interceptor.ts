import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { Store } from "@ngxs/store";
import { Signin } from 'src/app/shared/stores/actions/auth-action';
import { tap } from "rxjs/operators";

@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {
  jwtToken : String = "";

  constructor(private router: Router, private store: Store) { }

  intercept<T>(req: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> {
      console.log(`Token : ${this.jwtToken}`)
      if (this.jwtToken != "") {
          req = req.clone({ setHeaders: { Authorization: `Bearer ${this.jwtToken}` } });
      }

      return next.handle(req).pipe(tap(
          (evt: HttpEvent<any>) => {
              if (evt instanceof HttpResponse) {
                    let body = JSON.parse(JSON.stringify(evt.body));
                    if (body["token"] != undefined) 
                    {
                        this.jwtToken = body["token"];
                        this.store.dispatch(new Signin({
                            token: this.jwtToken
                        }));
                    }
              }
          },
          (error: HttpErrorResponse) => {
              switch (error.status) {
                  case 400:
                  case 401:
                      this.store.dispatch(new Signin({ token: undefined }));
                      this.router.navigate(['/signin']);
                      break;
              }
              return of(null);
          }
      ));
  }
}

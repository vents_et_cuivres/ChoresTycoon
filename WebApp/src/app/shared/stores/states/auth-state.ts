import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { Signin, Signout } from '../actions/auth-action';
import { AuthStateModel } from './auth-state-model';

@State<AuthStateModel>({
    name: "auth",
    defaults: {
        token: undefined
    }
})
@Injectable()
export class AuthState {
    
    @Selector()
    static isConnected(state: AuthStateModel): boolean {
        return state.token != undefined;
    }

    @Action(Signin)
    add(
        { patchState }: StateContext<AuthStateModel>,
        action: Signin
    ) {
        let newState: AuthStateModel = {
            token: action.payload.token
        };

        patchState(newState);
    }

    @Action(Signout)
    del(
        { patchState }: StateContext<AuthStateModel>
    ) {
        console.log("Logout !");
        let newState: AuthStateModel = {
            token: undefined
        };

        patchState(newState);
    }
}
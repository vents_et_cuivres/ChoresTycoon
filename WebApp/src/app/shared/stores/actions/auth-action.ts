export class Signin {
    static readonly type = "[Auth] Signin";
    constructor(public payload: { token?: String }) { }
}
export class Signout {
    static readonly type = "[Auth] Signout";
    constructor() { }
}
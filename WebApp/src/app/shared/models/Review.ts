export interface Review {
    id: string;
    choreId: string;
    memberId: string;
    reviewScore: number;
}
export interface Member {
    id: string,
    surname: string;
    name: string;
}
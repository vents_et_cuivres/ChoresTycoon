import { Member } from "./Member";
import { Review } from "./Review";

export interface Chore {
    id: string,
    name: string;
    description: string;
    deadline: string;
    completed: boolean;
    homeId: string;
    assignedMembers : Array<Member>;
    reviews: Array<Review>;
}
import { Chore } from "./Chore";
import { Member } from "./Member";

export interface Home {
    id: string;
    name: string;
    members: Array<Member>;
    chores: Array<Chore>;
}